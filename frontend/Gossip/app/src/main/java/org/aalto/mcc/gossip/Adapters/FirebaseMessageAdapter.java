package org.aalto.mcc.gossip.Adapters;

import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import org.aalto.mcc.gossip.Models.Message;
import org.aalto.mcc.gossip.R;

import java.util.ArrayList;
import java.util.List;

public class FirebaseMessageAdapter extends MessagesListAdapter {
    private List<String> imageMessages;
    public FirebaseMessageAdapter(String senderId, ImageLoader imageLoader) {
        super(senderId,imageLoader);
        imageMessages = new ArrayList<String>();
    }

    public void receivedNewMessage(IMessage message){
        super.addToStart(message,true);
        if(((Message)message).getImageUrl() != null && ((Message)message).getImageUrl() != "https://i.imgur.com/AnJI9QA.gif"){
            imageMessages.add(((Message)message).getImageUrl());
        }
    }

    @Override
    public void addToStart(IMessage message,boolean scroll) {
        super.addToStart(message, true);
        Message m = (Message) message;
        if(((Message)message).getImageUrl() != null && ((Message)message).getImageUrl() != "https://i.imgur.com/AnJI9QA.gif"){
            imageMessages.add(m.getImageUrl());
        }
    }

    public List<String> getImageMessages(){
        return imageMessages;
    }
}
