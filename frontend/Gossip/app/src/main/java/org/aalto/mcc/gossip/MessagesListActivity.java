package org.aalto.mcc.gossip;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.ml.vision.cloud.label.FirebaseVisionCloudLabel;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import org.aalto.mcc.gossip.Adapters.FirebaseMessageAdapter;
import org.aalto.mcc.gossip.Models.FirebaseConstants;
import org.aalto.mcc.gossip.Models.Message;
import org.aalto.mcc.gossip.Models.User;
import org.aalto.mcc.gossip.util.Backend;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  This activity shows message history of a group chat.
 *  Add groupID as intent data.
 */
public class MessagesListActivity extends AppCompatActivity implements MessagesListAdapter.OnLoadMoreListener {

    private static final int TOTAL_MESSAGES_COUNT = 100;

    private String groupID;
    private User currentUser;
    private Map<String, User> allUsers = new HashMap<>();

    private MessagesList messagesList;
    private FirebaseMessageAdapter messageAdapter;

    private MessageInput messageInput;
    private ImageButton attachmentButton;
    private EditText messageInputEditText;
    private ImageButton messageSendButton;

    private ImageLoader imageLoader;
    private String currentUserId;

    private ImageButton messagesListMenu;

    private Uri imageUri;

    private Picasso picassoGoogleStorageImageLoader;

    private ImageButton btn_goback;
    private ListenerRegistration updateUnreadCountListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        messagesList = findViewById(R.id.messagesList);
        messageInput = findViewById(R.id.input);
        attachmentButton = findViewById(R.id.attachmentButton);
        messageSendButton = findViewById(R.id.messageSendButton);
        messageInputEditText = findViewById(R.id.messageInput);
        messagesListMenu = findViewById(R.id.imgbtn_ml_chat_menu);
        btn_goback = findViewById(R.id.imgbtn_ml_goback);

        groupID = getIntent().getStringExtra("groupID");
        currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        messageAdapter = new FirebaseMessageAdapter(currentUserId, getImageLoader());
        messagesList.setAdapter(messageAdapter);
        currentUser = getCurrentUser();
        picassoGoogleStorageImageLoader = new Picasso.Builder(MessagesListActivity.this)
                .addRequestHandler(new FirebaseRequestHandler()).build();
        addListeners();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        // Resets the unread counter to zero as long as this group is in view.
        updateUnreadCountListener = updateUnreadCount();
    }

    @Override
    protected void onPause() {
        super.onPause();

        updateUnreadCountListener.remove();
    }

    private void receiveMessages() {
        Task<QuerySnapshot> allUsersQuery = FirebaseFirestore.getInstance()
                .collection("user_profiles")
                .get();


        // First retrieve all users to populate the allUsers field
        allUsersQuery.addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                    User user = new User(document.getId(), (String) document.get("display_name"), false);
                    allUsers.put(document.getId(), user);
                }

                final DocumentReference group = FirebaseFirestore.getInstance()
                        .collection("groups")
                        .document(groupID);

                Task<DocumentSnapshot> currentUserAsMember = group
                        .collection("members")
                        .document(currentUserId)
                        .get();

                // Then retrieve the current users member status, since we need the joined_on date to execute queries.
                currentUserAsMember.addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot document) {
                        Date joinedOn = document.getDate("joined_on");

                        if (joinedOn == null) {
                            // @todo do something when this happens
                            Log.e("@@@", "Joined_on wasn't found");
                            return;
                        }

                        Query lastMessageQuery = group
                                .collection("messages")
                                .orderBy("created", Query.Direction.ASCENDING)
                                .whereGreaterThan("created", joinedOn) // This is critical: otherwise the access rules block our request.
                                ;

                        // Add a listener for changes in the messages of this group, the first snapshot will contain all messages as it's diff.
                        lastMessageQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@javax.annotation.Nullable QuerySnapshot snapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                if (e != null) {
                                    Log.w("@@@", "Listen failed.", e);
                                    return;
                                }

                                // Process the messages which have changes.
                                for (DocumentChange doc: snapshots.getDocumentChanges()) {
                                    IMessage message = queryDocumentToMessage(doc.getDocument());
                                    if (message == null) {
                                        continue;
                                    }
                                    if (doc.getType() == DocumentChange.Type.ADDED) {
                                        messageAdapter.receivedNewMessage(message);
                                    } else if (doc.getType() == DocumentChange.Type.MODIFIED) {
                                        messageAdapter.update(message);
                                    } else if (doc.getType() == DocumentChange.Type.REMOVED) {
                                        messageAdapter.delete(message);
                                    } else {
                                        Log.e("@@@", "Message modified with unrecognized type " + doc.getType());
                                    }
                                }
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("@@@", "Couldn't retrieve current user's member data of group " + groupID);
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("@@@", "Could not retrieve all users");
            }
        });
    }

    @NonNull
    private IMessage queryDocumentToMessage(QueryDocumentSnapshot message) {
        String body = (String) message.get("body");
        Date created = message.getDate("created");
        if (created == null) { // Happens when the data is not yet written to the database.
            Log.d("@@@", "Local message");
            created = new Date();
        }
        Message m = new Message(message.getId(), allUsers.get(message.get("author")), body, created);
        if (message.getString("type") == null) {
            return null;
        }
        switch (message.getString("type")) {
            case "image":
                String label = (String) message.get("feature");
                Log.e("@@@", "queryDocumentToMessage: " + label);
                if (label == null) {
                    label = "other";
                }
                m.setImage(new Message.Image(FirebaseConstants.getChatImagesPath(MessagesListActivity.this,groupID, message.getId()), label));
                Log.e("@@@", "queryDocumentToMessage: " + FirebaseConstants.getChatImagesPath(MessagesListActivity.this,groupID, message.getId()));
                break;
            case "image_pending":
                m.setImage(new Message.Image("https://i.imgur.com/AnJI9QA.gif", ""));
                break;
        }
        if((message.get("type")).equals("image")){
            String label = (String)message.get("feature");
            Log.e("@@@", "queryDocumentToMessage: "+label);
            if(label==null){
                label="other";
            }
            String chatImagesPath = FirebaseConstants.getChatImagesPath(MessagesListActivity.this, groupID, message.getId());
            m.setImage(new Message.Image(chatImagesPath,label));
            Log.e("@@@", "queryDocumentToMessage: " + chatImagesPath);
        }
        return m;
    }

    private void addListeners(){
        // Receives all messages, both the initial ones and any future message.
        receiveMessages();

        // Invoked when the message send button is clicked
        messageSendButtonListener();

        btn_goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MessagesListActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Listens if the unread counter is raised while this view is open, if so is it again reset to zero.
     */
    private ListenerRegistration updateUnreadCount() {
        DocumentReference userToGroupDocument = FirebaseFirestore.getInstance()
                .collection("user_to_group")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .collection("groups")
                .document(groupID);

        return userToGroupDocument
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable DocumentSnapshot snapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (snapshot != null) {
                            Long unreadCount = snapshot.getLong("unread_count");
                            if (unreadCount != null && unreadCount > 0) {
                                userToGroupDocument.update("unread_count", 0);
                            }
                        }
                    }
        });
    }

    private void messageSendButtonListener() {
        messageSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Pair<Task<Void>, DocumentReference> task = Backend.postTextMessage(groupID, messageInputEditText.getText().toString());
                messageInputEditText.setText(""); // Clear the input after the user send the message.
                task.first.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MessagesListActivity.this, "Failed to send message", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        messagesListMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MessagesListActivity.this, MessagesListMenuFragment.class);
                intent.putExtra("groupID",groupID);
                intent.putStringArrayListExtra("images",(ArrayList<String>) messageAdapter.getImageMessages());
                startActivityForResult(intent,Requests.REQUEST_EXIT);
                //startActivity(intent);
            }
        });
        attachmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchChoosePictureOrTakePictureActivity();
                Log.e("@@@", "onClick: "+"clicked");
            }
        });
    }

    private void launchChoosePictureOrTakePictureActivity(){
        Intent pickIntent = new Intent();
        pickIntent.setType("image/*");
        String pickTitle = "Take picture or choose from gallery";
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File imageFile = Utils.createImageFile(MessagesListActivity.this);
        imageUri = FileProvider.getUriForFile(MessagesListActivity.this,BuildConfig.APPLICATION_ID+".provider", imageFile);
        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,new Intent[]{takePicture,pickPhoto});

        startActivityForResult(chooserIntent,Requests.CHOOSE_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode!=Activity.RESULT_OK){
            return;
        }
        switch (requestCode){
            case Requests.CHOOSE_IMAGE: {
                if(data!=null&&data.getData()!=null){
                    imageUri = data.getData();
                }
                sendImageMessage();
            } break;
        }
        if (requestCode == Requests.REQUEST_EXIT) {
            if (resultCode == RESULT_OK) {
                this.finish();
            }
        }
    }

    private void sendImageMessage(){
        try{
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);

            final Pair<Task<Void>, DocumentReference> imageUploadingTask = Backend.postImagePendingMessage(MessagesListActivity.this, groupID, bitmap);
            ImageLabelingTask labelingTask = new ImageLabelingTask(bitmap, imageUploadingTask.second);
            labelingTask.getResult().addOnCompleteListener(new OnCompleteListener<List<FirebaseVisionCloudLabel>>() {
                @Override
                public void onComplete(@NonNull Task<List<FirebaseVisionCloudLabel>> task) {
                    if(task.isSuccessful()){
                        final FirebaseVisionCloudLabel label = task.getResult().get(0);//get label here
                      
                        Backend.postImageLabel(groupID, imageUploadingTask.second.getId(), label.getLabel()).first.addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d("Labeling","Succeed add label to image");
                                    Log.d("Label", "onComplete: "+ label);
                                    messageAdapter.getImageMessages().add(FirebaseConstants.getChatImagesPath(MessagesListActivity.this, groupID, imageUploadingTask.second.getId()));
                                } else {
                                    Log.d("Labeling","Failed to add label to image", task.getException());
                                }
                            }
                        });
                    }
                }
            });
            imageUploadingTask.first.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.e("@@@", "onSuccess: "+imageUploadingTask.second.getId());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // TODO error handling
                    Log.e("@@@", "onSuccess: ", e);
                }
            });
        } catch (IOException e) {
            Log.e("@@@", "onActivityResult: ", e);
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        if (totalItemsCount < TOTAL_MESSAGES_COUNT) {
            // TODO load message history
        }
    }

    private ImageLoader getImageLoader() {
        if (imageLoader == null) {
            imageLoader = new ImageLoader() {
                @Override
                public void loadImage(ImageView imageView, @Nullable String url, @Nullable Object payload) {
                    Log.e("@@@", "loadImage: "+url);
                    picassoGoogleStorageImageLoader.load(url).into(imageView);
                }
            };
        }
        return imageLoader;
    }

    private User getCurrentUser(){
        Backend.getUserDetailById(FirebaseAuth.getInstance().getCurrentUser().getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                currentUser =  new User(
                        FirebaseAuth.getInstance().getCurrentUser().getUid(),
                        documentSnapshot.getString("display_name"),
                        false
                );
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // TODO error handling
            }
        });
        return currentUser;
    }
}