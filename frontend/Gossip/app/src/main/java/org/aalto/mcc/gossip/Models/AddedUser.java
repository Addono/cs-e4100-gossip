package org.aalto.mcc.gossip.Models;

public class AddedUser {

    // added user name
    private String username;

    // added userid
    private String userid;

    public AddedUser(String username, String userid) {
        super();
        this.username = username;
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserid(){
        return userid;
    }

    public void setUserid(String userid){
        this.userid = userid;
    }

}
