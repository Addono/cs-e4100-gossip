package org.aalto.mcc.gossip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.aalto.mcc.gossip.util.Backend;
import org.aalto.mcc.gossip.util.messaging.GossipMessagingService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {
    private Button loginButton;
    private Button signUpButton;
    private EditText emailEditText;
    private EditText passwordEditText;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginButton = findViewById(R.id.btn_login);
        signUpButton = findViewById(R.id.btn_signUp);
        emailEditText = findViewById(R.id.etxt_email);
        passwordEditText = findViewById(R.id.etxt_password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Signing in...");
        addListeners();
    }

    private void addListeners() {
        /*
         *  Add Login button listeners
         */
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                // show error for empty password, email
                if (email.isEmpty() || password.isEmpty()) {
                    Utils.createErrorMessage(LoginActivity.this, "Error", "Email or password can not be empty").show();
                } else {
                    loginUser(email, password);
                }
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchSignUpActivity();
            }
        });
    }

    private void loginUser(String email, String password) {
        progressDialog.show();
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .continueWithTask(task -> {
                    String userId = Objects.requireNonNull(task.getResult()).getUser().getUid();
                    return Backend.pushDeviceToken(userId);
                })
                .addOnSuccessListener(task -> {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    Log.i("@@@", "signInWithEmail:success; UID->" + Objects.requireNonNull(user).getUid());
                    setResult(Activity.RESULT_OK);
                    finish();
                    progressDialog.dismiss();
                })
                .addOnFailureListener(err -> {
                    // show error for wrong password, email
                    Log.e("LoginActivity", err.getMessage(), err);
                    Utils.createErrorMessage(LoginActivity.this, "Error", "Wrong username or password").show();
                    progressDialog.dismiss();
                });
    }

    private void launchSignUpActivity() {
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivityForResult(intent, Requests.SIGNUP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case Requests.CHOOSE_IMAGE: {
                processChooseImageResult(resultCode, data);
            }
            break;
            case Requests.SIGNUP: {
                if (requestCode == Activity.RESULT_OK) {
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }
            break;
        }
    }

    private void processChooseImageResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            try {
                InputStream inputStream = getBaseContext().getContentResolver().openInputStream(data.getData());
                ((ImageView) findViewById(R.id.imgview_userPhoto)).setImageDrawable(overlay(BitmapFactory.decodeStream(inputStream)));
            } catch (IOException e) {
                Log.e("@@@", "onActivityResult: ", e);
            }
        }
    }

    // add 'change photo' layer to profile photos
    private Drawable overlay(Bitmap bmp) {
        Drawable[] layers = new Drawable[2];
        layers[0] = new BitmapDrawable(getResources(), bmp);
        layers[1] = ContextCompat.getDrawable(LoginActivity.this, R.drawable.transparent_choose_photo);
        return new LayerDrawable(layers);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Toast.makeText(ChatActivity.this, "Return Disabled", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
