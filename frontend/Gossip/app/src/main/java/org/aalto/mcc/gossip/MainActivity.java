package org.aalto.mcc.gossip;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.aalto.mcc.gossip.util.messaging.GossipMessagingService;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class MainActivity extends AppCompatActivity {
    private static FirebaseAuth mAuth;
    private static StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void onStart() {
        super.onStart();
        GossipMessagingService.ensureGooglePlayServicesAvailable(this, this)
                .addOnSuccessListener(task -> {
                    if (getUser() == null) {
                        launchLoginActivity();
                    } else {
                        /* User is authenticated navigate to chat history
                         * startActivity(new ChatActivity());
                         */
                        launchChatActivity();
                    }
                })
                .addOnFailureListener(err -> Utils.googlePlayServiceErrorHandler(this, "MainActivity", err));
        // Check if user is signed in (non-null) and update UI accordingly.
    }

    @Override
    protected void onResume() {
        super.onResume();
        GossipMessagingService.ensureGooglePlayServicesAvailable(this, this)
                .addOnFailureListener(err -> Utils.googlePlayServiceErrorHandler(this, "MainActivity", err));
    }

    private void launchChatActivity() {
        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
        startActivity(intent);
    }

    private void launchLoginActivity() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivityForResult(intent, Requests.LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Requests.LOGIN:
                if (resultCode == Activity.RESULT_OK) {
                    launchChatActivity();
                }
                break;
        }
    }

    /**
     * Returns FirebaseAuth instance
     *
     * @return
     */
    public static FirebaseAuth getFirebaseAuth() {
        if (mAuth == null) {
            mAuth = FirebaseAuth.getInstance();
        }
        return mAuth;
    }

    /**
     * Rerurns current FirebaseUser
     *
     * @return
     */
    public static FirebaseUser getUser() {
        return getFirebaseAuth().getCurrentUser();
    }

    public static StorageReference getStorageReference() {
        if (storageReference == null) {
            storageReference = FirebaseStorage.getInstance().getReference();
        }
        return storageReference;
    }
}
