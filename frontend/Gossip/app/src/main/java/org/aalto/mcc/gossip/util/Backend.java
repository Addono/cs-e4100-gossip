package org.aalto.mcc.gossip.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.ml.vision.cloud.label.FirebaseVisionCloudLabel;
import com.google.firebase.ml.vision.cloud.label.FirebaseVisionCloudLabelDetector;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.aalto.mcc.gossip.MainActivity;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.content.ContentValues.TAG;

public class Backend {

    /**
     * Registers the currently authenticated user also in the Firestore database.
     *
     * @param displayName The display name of this user.
     * @return The future of the database.
     */
    @NonNull
    public static Task<Void> registerUser(String displayName) {
        return updateDisplayName(displayName);
    }

    /**
     * Updates the display name for the current user.
     *
     * @param displayName The name to be set as the display name.
     * @return The future of the database.
     */
    @NonNull
    public static Task<Void> updateDisplayName(String displayName) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> user = new HashMap<>();
        user.put("display_name", displayName);

        return db.collection("user_profiles")
                .document(mAuth.getCurrentUser().getUid())
                .set(user);
    }

    /**
     * Create a new group with only the current user as member.
     *
     * @return A pair consisting of the future of the batch create group commit to the database and
     * a document reference to the newly created group (reference is not present in the
     * database until the future succeeds).
     */
    public static Pair<Task<Void>, DocumentReference> createGroup() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        WriteBatch batch = db.batch();

        // Create a new group with the current user as the only member.
        final DocumentReference group = db.collection("groups").document(); // The fresh document of our new group.
        final DocumentReference member = group.collection("members").document(mAuth.getCurrentUser().getUid());
        Map<String, Object> joinedOn = new HashMap<>();
        joinedOn.put("joined_on", FieldValue.serverTimestamp());

        batch.set(member, joinedOn); // Queue for writing

        // Update the userToGroup mapping.
        DocumentReference userToGroupMapping = db.collection("user_to_group").document(mAuth.getCurrentUser().getUid());
        DocumentReference key = userToGroupMapping.collection("groups").document(group.getId());

        Map<String, Object> exists = new HashMap<>();
        exists.put("is_member", true);

        batch.set(key, exists);

        return new Pair<>(batch.commit(), group);
    }

    /**
     * Adds a user to a group in Firestore.
     *
     * @param userId  The uid of the user to add.
     * @param groupId The uid of the group to add {@code user} to.
     * @return The future of the database.
     */
    public static Task<Void> addUserToGroup(String userId, String groupId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        WriteBatch batch = db.batch();

        final DocumentReference group = db.collection("groups").document(groupId);
        final DocumentReference member = group.collection("members").document(userId);

        Map<String, Object> joinedOn = new HashMap<>();
        joinedOn.put("joined_on", FieldValue.serverTimestamp());

        batch.set(member, joinedOn); // Queue for writing

        // Update the userToGroup mapping.
        DocumentReference userToGroupMapping = db.collection("user_to_group").document(userId);
        DocumentReference key = userToGroupMapping.collection("groups").document(group.getId());

        Map<String, Object> exists = new HashMap<>();
        exists.put("is_member", true);

        batch.set(key, exists);

        return batch.commit();
    }

    /**
     * Posts a text message to a group.
     *
     * @param groupId The uid of the group to post the message to.
     * @param message The plain text version of the message.
     * @return A {@link Pair} of the future of the database and a reference to the newly created message in the database.
     */
    public static Pair<Task<Void>, DocumentReference> postTextMessage(String groupId, String message) {
        return postMessage(groupId, "text", message);
    }

    /**
     * Creates an image_pending message type in the database. Future versions will also accept an
     * image and upload this to Firestore Storage for further processing.
     *
     * @param groupId The group to add this new message to.
     * @param image   The image as PNG stored in a {@code Bitmap} format.
     * @return A {@link Pair} of the future of the database and a reference to the newly created message in the database.
     */
    public static Pair<Task<Void>, DocumentReference> postImagePendingMessage(final Context context, final String groupId, final Bitmap image) {
        final Pair<Task<Void>, DocumentReference> result = postMessage(groupId, "image_pending", null);

        result.first.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                String path = "groups/" + groupId + "/messages/" + result.second.getId() + "/image.jpg";
                uploadImage(image, path, context);
            }
        });
        return result;
    }

    /**
     * Put the label to database
     *
     * @param groupId The group to add this new message to.
     * @param msgId   The message to this image belongs to.
     * @param label   the label of this image
     * @return A {@link Pair} of the future of the database and a reference to the newly created message in the database.
     */
    public static Pair<Task<Void>, DocumentReference> postImageLabel(String groupId, String msgId, String label) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        WriteBatch batch = db.batch();
        final DocumentReference imageMessage = db.collection("groups")
                .document(groupId)
                .collection("messages").document(msgId);
        batch.update(imageMessage, "feature", label);
        return new Pair<>(batch.commit(), imageMessage);


    }

    private static Pair<Task<Void>, DocumentReference> postMessage(String groupId, String type, String body) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        final DocumentReference group = db.collection("groups").document(groupId);
        final DocumentReference messages = group.collection("messages").document();
        Map<String, Object> dbMessage = new HashMap<>();
        dbMessage.put("type", type);
        dbMessage.put("created", FieldValue.serverTimestamp());
        dbMessage.put("author", mAuth.getUid());
        if (body != null) {
            dbMessage.put("body", body);
        }

        return new Pair<>(messages.set(dbMessage), messages);
    }

    /**
     * Removes the currently logged in user from a specific group.
     *
     * @param groupId The uid of the group.
     * @return The future of the database.
     */
    public static Task<Void> leaveChat(String groupId) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        return removeUserFromChat(groupId, mAuth.getCurrentUser().getUid());
    }

    private static Task<Void> removeUserFromChat(String groupId, String userId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        WriteBatch batch = db.batch();

        // Create a new group with the current user as the only member.
        final DocumentReference group = db.collection("groups").document(groupId);
        final DocumentReference member = group.collection("members").document(userId);

        batch.delete(member); // Queue for writing

        // Update the userToGroup mapping.
        DocumentReference userToGroupMapping = db.collection("user_to_group").document(userId);
        DocumentReference key = userToGroupMapping.collection("groups").document(groupId);

        batch.delete(key);

        return batch.commit();
    }

    public static Task<QuerySnapshot> getConversationById(String userId) throws NullPointerException {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference userToGroupMapping = db.collection("user_to_group").document(userId).collection("group");
        return userToGroupMapping.get();
    }

    public static Task<DocumentSnapshot> getUserDetailById(String userId) throws NullPointerException {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference user = db.collection("user_profiles").document(userId);
        return user.get();
    }


    public static Task<QuerySnapshot> getGroupMember(String groupId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference user = db.collection("groups").document(groupId).collection("members");
        return user.get();
    }

    public static Task<QuerySnapshot> getLastMessage(String groupId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference groupInfoRef = db.collection("groups").document(groupId);
        return groupInfoRef.collection("messages")
                .orderBy("joined_on", Query.Direction.DESCENDING)
                .limit(1)
                .get();
    }

    public static Task<QuerySnapshot> getAllUsers() {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("user_profiles").get();
    }

    public static Task<DocumentSnapshot> getUserProfilePhoto(String userId) {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("profile_pictures").document(userId).get();
    }


    public static Task<List<FirebaseVisionCloudLabel>> imageLabeling(FirebaseVisionImage image,
                                                                     final DocumentReference imageDbRef,
                                                                     FirebaseVisionCloudLabelDetector detector) {

        return detector.detectInImage(image)
                .addOnSuccessListener(
                        new OnSuccessListener<List<FirebaseVisionCloudLabel>>() {
                            @Override
                            public void onSuccess(List<FirebaseVisionCloudLabel> labels) {
                                boolean isOthersFlag = true;
                                for (FirebaseVisionCloudLabel label : labels) {
                                    Log.i(TAG, "Image has Label: " + label.getLabel());
                                    //Add Labels to Database
                                    if(label.getLabel().equals("Technology")){
                                        imageDbRef.update("feature", "Technology");
                                        isOthersFlag = false;
                                        break;
                                    } else if(label.getLabel().contains("Food")){
                                        imageDbRef.update("feature", "Food");
                                        isOthersFlag = false;
                                        break;
                                    } else if(label.getLabel().contains("Screenshot")){
                                        imageDbRef.update("feature", "Screenshots");
                                        isOthersFlag = false;
                                        break;
                                    }

                                }
                                if (isOthersFlag) imageDbRef.update("feature", "Others");

                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e(TAG, "Labeling error");
                            }
                        });
    }

    /**
     * Get the label of image.
     *
     * @param groupId,msgId the current groupID and messageId
     * @return The future of the label of this image.
     */

    public static Task<QuerySnapshot> getLabelImage(String groupId, String msgId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("groups")
                .document(groupId)
                .collection("messages").document(msgId).collection("feature").get();
    }

    private static UploadTask uploadImage(Bitmap image, String path, Context context) {
        image = rescaleImage(image, context);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 80, stream);

        StorageReference imageRef = MainActivity.getStorageReference().child(path);
        return imageRef.putBytes(stream.toByteArray());
    }

    private static Bitmap rescaleImage(Bitmap image, Context context) {
        String quality = context == null ? "full" : context.getSharedPreferences("res", Context.MODE_PRIVATE).getString("resolution", "full");

        int boundWidth;
        int boundHeight;

        switch (quality) {
            case "low":
                boundWidth = 640;
                boundHeight = 480;
                break;
            case "high":
                boundWidth = 1280;
                boundHeight = 960;
                break;
            case "full":
            default:
                boundWidth = 5000;
                boundHeight = 5000;
                break;
        }

        int originalWidth = image.getWidth();
        int originalHeight = image.getHeight();
        int newWidth = originalWidth;
        int newHeight = originalHeight;
        // First check if we need to scale width
        if (originalWidth > boundWidth) {
            // Scale width to fit
            newWidth = boundWidth;
            // Scale height to maintain aspect ratio
            newHeight = (newWidth * originalHeight) / originalWidth;
        }

        // then check if we need to scale even with the new height
        if (newHeight > boundHeight) {
            // Scale height to fit instead
            newHeight = boundHeight;
            // Scale width to maintain aspect ratio
            newWidth = (newHeight * originalWidth) / originalHeight;
        }

        image = Bitmap.createScaledBitmap(image, newWidth, newHeight, false);
        return image;
    }

    /**
     * Updates the users profile picture with another one.
     *
     * @param image The image to be uploaded as the user profile.
     * @return The future of the storage backend.
     */
    public static UploadTask updateProfilePicture(Bitmap image) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String path = "profile_pictures/" + mAuth.getUid() + "/profile_picture.jpg";

        return uploadImage(image, path, null);
    }


    public static Task<Void> pushDeviceToken(final String userId) {
        return FirebaseInstanceId
                .getInstance()
                .getInstanceId()
                .continueWithTask(task -> {
                    final String deviceToken = Objects.requireNonNull(task.getResult()).getToken();
                    return Backend.pushDeviceToken(userId, deviceToken).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("device_token", "Added device token to backend");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("device_token", "Couldn't add device token to firestore: ", e);
                        }
                    });
                });
    }

    public static Task<Void> pushDeviceToken(String userId, final String deviceToken) {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        final DocumentReference userDeviceTokensReference = db.collection("device_tokens").document(userId);
        final Map<String, Object> userDeviceTokens = new HashMap<>();
        userDeviceTokens.put("tokens", FieldValue.arrayUnion(deviceToken));
        return userDeviceTokensReference.set(userDeviceTokens);
    }

    public static Task<Void> removeDeviceToken(final String userId) {
        return FirebaseInstanceId
                .getInstance()
                .getInstanceId()
                .continueWithTask(task -> {
                    final String deviceToken = Objects.requireNonNull(task.getResult()).getToken();
                    return Backend.removeDeviceToken(userId, deviceToken);
                });
    }

    private static Task<Void> removeDeviceToken(String userId, final String deviceToken) {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        final DocumentReference userDeviceTokensReference = db.collection("device_tokens").document(userId);
        return userDeviceTokensReference.update("tokens", FieldValue.arrayRemove(deviceToken));
    }

    public static Task<DocumentSnapshot> getMessageInfo(String groupId, String messageId){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        return  db.collection("groups")
                .document(groupId).collection("messages").document(messageId).get();
    }
}
