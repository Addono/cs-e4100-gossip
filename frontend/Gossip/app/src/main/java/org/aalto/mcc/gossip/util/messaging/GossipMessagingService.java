package org.aalto.mcc.gossip.util.messaging;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.aalto.mcc.gossip.MessagesListActivity;
import org.aalto.mcc.gossip.Models.PushNotificationType;
import org.aalto.mcc.gossip.R;
import org.aalto.mcc.gossip.util.Backend;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class GossipMessagingService extends FirebaseMessagingService {

    private static Map<String, Integer> notificationChannels = new HashMap<>();
    private static AtomicInteger channelIdGenerator = new AtomicInteger();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData() != null && !remoteMessage.getData().isEmpty()) {
            Log.d("Push Noti", "Remote message received: " + remoteMessage.getData().get("body"));
            String groupId = remoteMessage.getData().get("groupId");
            PushNotificationType pushNotificationType = PushNotificationType.valueOf(remoteMessage.getData().get("push_notification_type"));
            Intent intent = new Intent(this, MessagesListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("groupID", groupId);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            String channelName = getChannelName(groupId, pushNotificationType);
            updateNotificationChannelMapIfNotExist(channelName);
            int nid1 = notificationChannels.get(channelName);
            int nid2 = channelIdGenerator.incrementAndGet();
            Log.d("@@@ eper", Integer.toString(nid1));
            Log.d("@@@ eper", channelName);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            Notification groupBuilder = new NotificationCompat.Builder(this, channelName)
                    .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                    .setSmallIcon(R.drawable.logo_white)
                    .setGroupSummary(true)
                    .setGroup(channelName)
                    .setContentTitle(remoteMessage.getData().get("title"))
                    .setContentText(remoteMessage.getData().get("body"))
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setContentIntent(pendingIntent)
                    .build();

            Notification notification = new NotificationCompat.Builder(this, channelName)
                    .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                    .setGroup(channelName)
                    .setSmallIcon(R.drawable.logo_white)
                    .setContentTitle(remoteMessage.getData().get("title"))
                    .setContentText(remoteMessage.getData().get("body"))
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setContentIntent(pendingIntent)
                    .build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Objects.requireNonNull(notificationManager).notify(nid1, groupBuilder);
            Objects.requireNonNull(notificationManager).notify(nid2, notification);
        }
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
        Log.d("Push Noti", "Some push messages have not been delivered to the app. Can't really do anything about it");
    }

    @Override
    public void onNewToken(String newDeviceToken) {
        super.onNewToken(newDeviceToken);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            Backend.pushDeviceToken(user.getUid(), newDeviceToken);
        } else {
            Log.d("Device token", "Refreshed token, but not logged in: " + newDeviceToken);
        }
    }

    private String getChannelName(String groupId, PushNotificationType pushNotificationType) {
        if (pushNotificationType.equals(PushNotificationType.added_to_group)) {
            return getResources().getString(R.string.added_to_group_notification, groupId);
        } else {
            return getResources().getString(R.string.new_message_notification, groupId);
        }
    }

    private void updateNotificationChannelMapIfNotExist(String channelName) {
        if (!notificationChannels.containsKey(channelName)) {
            int nextChannelId = channelIdGenerator.getAndIncrement();
            notificationChannels.put(channelName, nextChannelId);
        }
    }

    public static Task<Void> ensureGooglePlayServicesAvailable(Context context, Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        if (googleApiAvailability.isGooglePlayServicesAvailable(context) != ConnectionResult.SUCCESS) {
            CharSequence text = "Gossip is enabling Google Play Services. Application will restart.";
            int duration = Toast.LENGTH_LONG;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return googleApiAvailability.makeGooglePlayServicesAvailable(activity);
        } else {
            TaskCompletionSource<Void> taskCompletionSource = new TaskCompletionSource<>();
            taskCompletionSource.setResult(null);
            return taskCompletionSource.getTask();
        }
    }
}
