package org.aalto.mcc.gossip.Models;

public enum PushNotificationType {
    new_message,
    added_to_group
}
