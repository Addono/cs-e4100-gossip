package org.aalto.mcc.gossip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.storage.UploadTask;

import org.aalto.mcc.gossip.util.Backend;

import java.io.File;
import java.io.IOException;

public class SignUpActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    private Uri imageUri;
    private Button signUpButton;
    private ImageView profilePictureImageView;
    private EditText usernameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText passwordAgainEditText;
    private ProgressDialog progressDialog;
    private ImageButton btn_goback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        auth = MainActivity.getFirebaseAuth();
        profilePictureImageView = findViewById(R.id.imgview_userPhoto);
        usernameEditText = findViewById(R.id.etxt_username);
        emailEditText = findViewById(R.id.etxt_email);
        passwordEditText = findViewById(R.id.etxt_password);
        passwordAgainEditText = findViewById(R.id.etxt_passwordAgain);
        signUpButton = findViewById(R.id.btn_signUp);
        btn_goback = findViewById(R.id.imgbtn_su_goback);
        profilePictureImageView.setImageDrawable(overlay(ContextCompat.getDrawable(SignUpActivity.this, R.drawable.login_icon)));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Signing up...");
        addListeners();
    }

    private void addListeners(){
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUpUser();
            }
        });

        profilePictureImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchChoosePictureOrTakePictureActivity();
            }
        });

        btn_goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode){
            case Requests.CHOOSE_IMAGE:{
                processChooseImageResult(resultCode, data);
            }   break;
        }
    }

    private void processChooseImageResult(int resultCode, Intent data){
        if(resultCode == Activity.RESULT_OK){
            if(data != null && data.hasExtra("remove_image")){
                profilePictureImageView.setImageDrawable(overlay(ContextCompat.getDrawable(SignUpActivity.this, R.drawable.user_avatar)));
                imageUri = null;
                return;
            }
            boolean isCamera;
            if (data == null) {
                isCamera = true;
            } else {
                String action = data.getAction();
                if (action == null) {
                    isCamera = false;
                } else {
                    isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                }
            }

            if (!isCamera) {
                imageUri = data == null ? null : data.getData();
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                Bitmap resizedPhoto = Utils.resizeImage(bitmap, profilePictureImageView.getWidth(), profilePictureImageView.getHeight());
                Drawable profilePhoto = new BitmapDrawable(getResources(), resizedPhoto);
                profilePictureImageView.setImageDrawable(overlay(profilePhoto));
            } catch (IOException e) {
                Log.e("@@@",e.getMessage());
            }
        }
    }

    private void signUpUser(){
        final String email = emailEditText.getText().toString();
        final String username = usernameEditText.getText().toString();
        final String password = passwordEditText.getText().toString();
        final String passwordAgain = passwordAgainEditText.getText().toString();
        // check password match, fields are empty
        if(email.isEmpty() || password.isEmpty() || username.isEmpty() || !password.equals(passwordAgain)){
            if(!password.equals(passwordAgain)){
                Utils.createErrorMessage(SignUpActivity.this, "Error", "Passwords do not match").show();
            } else {
                Utils.createErrorMessage(SignUpActivity.this, "Error", "Email or password can not be empty").show();
            }
            return;
        }
        if(password.length() < 6){
            Utils.createErrorMessage(SignUpActivity.this, "Error", "The length of your password should be at least 6").show();
            return;
        }
        progressDialog.show();
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            // user registered successfully
            @Override
            public void onSuccess(AuthResult authResult) {
                Backend.registerUser(username).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // register user with profile photo
                        if (imageUri == null) {
                            progressDialog.dismiss();
                            Toast.makeText(SignUpActivity.this, "Registered successfully", Toast.LENGTH_SHORT).show();
                            setResult(Activity.RESULT_OK);
                            finish();
                        } else {
                            Bitmap bitmapProfilePhoto;
                            try{
                                bitmapProfilePhoto = MediaStore.Images.Media.getBitmap(SignUpActivity.this.getContentResolver(), imageUri);
                            } catch (IOException e){
                                Log.e("@@@", "Failed retrieving the image", e);
                                Toast.makeText(SignUpActivity.this, "Registered user, but failed to add profile picture", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                                return;
                            }
                            Backend.updateProfilePicture(bitmapProfilePhoto).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    progressDialog.dismiss();
                                    Toast.makeText(SignUpActivity.this, "Registered successfully", Toast.LENGTH_SHORT).show();
                                    setResult(Activity.RESULT_OK);
                                    finish();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    progressDialog.dismiss();
                                    Toast.makeText(SignUpActivity.this, "Registered user, but failed to add profile picture", Toast.LENGTH_LONG).show();
                                    setResult(Activity.RESULT_OK);
                                    finish();
                                }
                            });
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        System.out.println("Failed to add user");
                        Log.e("@@@", "Failed to register user", e);
                        progressDialog.dismiss();
                        Toast.makeText(SignUpActivity.this, "Oops! We are having trouble, please ty again later", Toast.LENGTH_LONG).show();
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                });
            }
        // user cant be registered, most possible problem duplicate email
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("@@@", "Failed to register user", e);
                progressDialog.dismiss();
                if(e instanceof FirebaseAuthUserCollisionException){
                    Toast.makeText(SignUpActivity.this, "This email address is already registered!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(SignUpActivity.this, "Oops! Check the required fields and try again", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void launchChoosePictureOrTakePictureActivity(){
        Intent pickIntent = new Intent();
        pickIntent.setType("image/*");
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File imageFile = Utils.createImageFile(SignUpActivity.this);
        imageUri = FileProvider.getUriForFile(SignUpActivity.this,BuildConfig.APPLICATION_ID+".provider", imageFile);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        String pickTitle = "Select or take a new Picture";
        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,new Intent[] { takePhotoIntent, new Intent(SignUpActivity.this,DeleteProfileImageActivity.class)});

        startActivityForResult(chooserIntent, Requests.CHOOSE_IMAGE);
    }

    private Drawable overlay(Drawable drawable) {
        Drawable[] layers = new Drawable[2];
        layers[0] =  drawable;
        layers[1] = ContextCompat.getDrawable(SignUpActivity.this, R.drawable.transparent_choose_photo);
        return new LayerDrawable(layers);
    }
}
