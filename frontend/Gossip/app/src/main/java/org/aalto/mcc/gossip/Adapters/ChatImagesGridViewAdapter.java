package org.aalto.mcc.gossip.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.aalto.mcc.gossip.FirebaseRequestHandler;
import org.aalto.mcc.gossip.R;

import java.util.List;

public class ChatImagesGridViewAdapter  extends BaseAdapter {
    private final Context context;
    private final List<String> imageURLs;
    private Picasso picasso;

    public ChatImagesGridViewAdapter(Context context, List<String> imageURLs) {
        this.context = context;
        this.imageURLs = imageURLs;
        picasso = new Picasso.Builder(context).addRequestHandler(new FirebaseRequestHandler()).build();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View container =  convertView;
        if (container== null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            container = inflater.inflate(R.layout.image_container, null);
        }

        // Get the image URL for the current position.
        String url = getItem(position);
        ImageView imgView =  container.findViewById(R.id.image);

        // Trigger the download of the URL asynchronously into the image view.
        picasso
                .load(url)
                .placeholder(R.drawable.image_holder)
                .into(imgView);

        return container;
    }

    @Override
    public int getCount() {
        return imageURLs.size();
    }

    @Override
    public String getItem(int position) {
        return imageURLs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



}
