package org.aalto.mcc.gossip.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.aalto.mcc.gossip.Models.Conversation;
import org.aalto.mcc.gossip.R;

import cn.bingoogolapple.badgeview.BGABadgeTextView;

import java.util.List;

public class ChatItemAdapter extends BaseAdapter{

    private List<Conversation> data;
    private Context mContext;

    public ChatItemAdapter(Context context, List<Conversation> data){
        this.data = data;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Conversation getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.chat_item, null);

            viewHolder = new ViewHolder();

            viewHolder.portrait = (ImageView) view.findViewById(R.id.chat_item_portrait);
            viewHolder.name = (TextView) view.findViewById(R.id.chat_item_name);
            viewHolder.time = (TextView) view.findViewById(R.id.chat_item_time);
            viewHolder.content = (TextView) view.findViewById(R.id.chat_item_content);
            viewHolder.messageNum = (BGABadgeTextView) view.findViewById(R.id.chat_item_message_num);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.init(getItem(i));

        return view;
    }


    private class ViewHolder {

        private ImageView portrait;

        private TextView name;

        private TextView time;

        private TextView content;

        private BGABadgeTextView messageNum;

        private void init(Conversation item) {
            int unread = item.getUnreadNum();

            portrait.setImageBitmap(item.getProfile());
            name.setText(item.getName());
            time.setText(item.getTime());
            content.setText(item.getContent());

            if (unread != 0) {
                messageNum.showCirclePointBadge();
                messageNum.showTextBadge(unread + "");
            }
        }
    }
}
