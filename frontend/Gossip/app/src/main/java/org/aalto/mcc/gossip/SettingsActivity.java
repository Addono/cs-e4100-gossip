package org.aalto.mcc.gossip;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.aalto.mcc.gossip.util.Backend;

import java.util.Objects;

public class SettingsActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;

    private Spinner spinner_resolution;

    private Button btn_logout;

//    private SharedPreferences resolutionPreferences = PreferenceManager
//            .getDefaultSharedPreferences(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        sharedPreferences = getSharedPreferences("res", Context.MODE_PRIVATE);
        spinner_resolution = (Spinner) findViewById(R.id.spinner_resolution);
        btn_logout = (Button) findViewById(R.id.btn_logout);

        spinner_resolution.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String[] resolutions = getResources().getStringArray(R.array.resolutions);
                Toast.makeText(SettingsActivity.this, "Selected: "+resolutions[i], Toast.LENGTH_SHORT).show();
                //store resolution locally
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("resolution", resolutions[i]);
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                final String currentUserId = Objects.requireNonNull(user).getUid();
                Backend.removeDeviceToken(currentUserId)
                        .addOnSuccessListener(aVoid -> {
                            FirebaseAuth.getInstance().signOut();
                            // go back to sign out when log out
//                            Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
//                            startActivity(intent);


                            exit();
                            //onDestroy();
                        }).addOnFailureListener(error -> {
                    Log.e("@@@",error.getMessage(), error);
                });

            }
        });
        setActionBar();
    }

    private void setActionBar() {
        ActionBar actionBar = getSupportActionBar();
        // display goback button
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setTitle("Settings");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void exit() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setClass(this, MainActivity.class);
        intent.putExtra("exitApp", true);

        startActivity(intent);
    }

}
