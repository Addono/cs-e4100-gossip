package org.aalto.mcc.gossip;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.aalto.mcc.gossip.Adapters.ChatImagesGridViewAdapter;

import java.util.List;
import java.util.Map;

public class ShowImagesByDate extends Fragment {
    private Map<String ,List<String>> images;

    private LinearLayout imagesLayout;

    public ShowImagesByDate() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        images = (Map<String, List<String>>) getArguments().getSerializable("images");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imagesLayout = getView().findViewById(R.id.grouped_images);

        createSubImageGroups();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.image_group_layout, container, false);
    }

    private void createSubImageGroups(){
        // TODO group images by date

        for (String key: images.keySet()){
            if(images.get(key) != null) {
                View view = getLayoutInflater().inflate(R.layout.image_gridview_layout, null);
                ((TextView) view.findViewById(R.id.header)).setText(key);
                GridView today = (GridView) view.findViewById(R.id.images_gridview);
                setAdpater(today, images.get(key));
                imagesLayout.addView(view);
            }
        }
    }

    public void setGridViewHeight(GridView gridview) {

        ListAdapter listAdapter = gridview.getAdapter();
        if (listAdapter == null) {
            return;
        }

        gridview.setColumnWidth(150);
        int item_num = listAdapter.getCount();
        int numColumns = item_num / 3 + item_num % 3;

        int totalHeight = 0;

        for (int i = 0; i < listAdapter.getCount(); i += numColumns) {
            View listItem = listAdapter.getView(i, null, gridview);
            listItem.measure(0, 0);
            // get total height
            totalHeight += listItem.getMeasuredHeight();
        }

        // get gridview parameter
        ViewGroup.LayoutParams params = gridview.getLayoutParams();
        // set height
        params.height = totalHeight + 120;
        // set parameter
        gridview.setLayoutParams(params);
    }

    public void setAdpater(GridView view, List<String> images){

        ChatImagesGridViewAdapter adapter = new ChatImagesGridViewAdapter(getContext(),images);
        view.setAdapter(adapter);
        setGridViewHeight(view);
        adapter.notifyDataSetChanged();

    }
}
