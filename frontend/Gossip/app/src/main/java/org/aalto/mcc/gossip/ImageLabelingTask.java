package org.aalto.mcc.gossip;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions;
import com.google.firebase.ml.vision.cloud.label.FirebaseVisionCloudLabel;
import com.google.firebase.ml.vision.cloud.label.FirebaseVisionCloudLabelDetector;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

import org.aalto.mcc.gossip.util.Backend;

import java.util.List;


public class ImageLabelingTask{
    private FirebaseVisionImage image;
    private static final String TAG = "Image Labeling";
    private final DocumentReference imageDbRef;
    private Task<List<FirebaseVisionCloudLabel>> result;


    ImageLabelingTask(Bitmap bitmap, DocumentReference imageDbRef) {
        image = FirebaseVisionImage.fromBitmap(bitmap);

        this.imageDbRef = imageDbRef;
    }

    public Task<List<FirebaseVisionCloudLabel>> getResult() {
         FirebaseVisionCloudDetectorOptions options = new FirebaseVisionCloudDetectorOptions.Builder()
                .setModelType(FirebaseVisionCloudDetectorOptions.LATEST_MODEL)
                .setMaxResults(10)
                .build();
        FirebaseVisionCloudLabelDetector detector = FirebaseVision.getInstance()
                .getVisionCloudLabelDetector(options);
        return Backend.imageLabeling(image, imageDbRef, detector);
    }
}
