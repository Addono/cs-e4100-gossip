package org.aalto.mcc.gossip.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.aalto.mcc.gossip.AddGroupActivity;
import org.aalto.mcc.gossip.Models.AddedUser;
import org.aalto.mcc.gossip.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddedUserAdapter extends BaseAdapter implements Filterable {

    private List<AddedUser> list = new ArrayList<AddedUser>();

    private Context mContext;
    // Map isCheck stores the check status of the data in each position of a list
    private Map<Integer, Boolean> isCheck = new HashMap<Integer, Boolean>();

    private ArrayFilter mFilter;

    private ArrayList<AddedUser> mOriginalValues;

    private final Object mLock = new Object();

    public boolean noFilter = true;

    public AddedUserAdapter(Context mContext) {
        super();
        this.mContext = mContext;
        // initialize every checkbox to be empty
        initCheck(false);
    }

    public void setNoFilter(boolean flag){
        noFilter = flag;
    }

    // initialize isCheck
    public void initCheck(boolean flag) {
        for (int i = 0; i < list.size(); i++) {
            isCheck.put(i, flag);
        }
    }

    // parse all checked user
    public List<AddedUser> checkedUser(){
        List<AddedUser> added = new ArrayList<AddedUser>();

        for (int key : isCheck.keySet()) {
            if(isCheck.get(key)){
                added.add(list.get(key));
            }
        }
        return added;
    }

    // set data
    public void setData(List<AddedUser> data) {
        this.list = data;
    }

    // add data
    public void addData(AddedUser user) {
        list.add(0, user);
    }

    @Override
    public int getCount() {
        if(noFilter){
            return 0;
        }
        return list != null ? list.size() : 0;
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        View view = null;

        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.added_user_item, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.tvTitle);
            viewHolder.cbCheckBox = (CheckBox) view
                    .findViewById(R.id.cbCheckBox);

            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        AddedUser bean = list.get(position);

        viewHolder.title.setText(bean.getUsername().toString());

        viewHolder.cbCheckBox
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // stores checked users to isCheck
                        isCheck.put(position, isChecked);
                    }
                });
        // set positions and status
        if (isCheck.get(position) == null) {
            isCheck.put(position, false);
        }
        viewHolder.cbCheckBox.setChecked(isCheck.get(position));
        return view;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }


    public static class ViewHolder {
        public TextView title;
        public CheckBox cbCheckBox;
    }


    public Map<Integer, Boolean> getMap() {

        return isCheck;
    }


    public void removeData(int position) {
        list.remove(position);
    }

    private class ArrayFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            // results stores filtered results
            FilterResults results = new FilterResults();
            //if original data backup is empty, lock and duplicate data
            if (mOriginalValues == null) {
                synchronized (mLock) {
                    mOriginalValues = new ArrayList<>(list);
                }
            }
            // if prefix is null, lock and duplicate data
            if (prefix == null || prefix.length() == 0) {
                ArrayList<AddedUser> tmplist;
                synchronized (mLock) {
                    tmplist = new ArrayList<>(mOriginalValues);
                }
                results.values = tmplist;
                results.count = tmplist.size();
            } else {
                String prefixString = prefix.toString().toLowerCase();

                ArrayList<AddedUser> values;
                synchronized (mLock) {
                    values = new ArrayList<>(mOriginalValues);
                }
                final int count = values.size();
                final ArrayList<AddedUser> newValues = new ArrayList<>();

                for (int i = 0; i < count; i++) {
                    final AddedUser value = values.get(i);
                    // filter username
                    final String valueText = value.getUsername().toLowerCase();
                    // First match against the whole, non-splitted value
                    if (valueText.startsWith(prefixString) || valueText.indexOf(prefixString.toString()) != -1) {
                        newValues.add(value);
                    } else {
                        final String[] words = valueText.split(" ");
                        final int wordCount = words.length;

                        // Start at index 0, in case valueText starts with space(s)
                        for (int k = 0; k < wordCount; k++) {
                            if (words[k].startsWith(prefixString)) {
                                newValues.add(value);
                                break;
                            }
                        }
                    }
                }
                results.values = newValues;
                results.count = newValues.size();
            }
            return results;
        }

        //Result after filtering
        @Override
        protected void publishResults(CharSequence prefix, FilterResults results) {
            //noinspection unchecked
            list = (List<AddedUser>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
