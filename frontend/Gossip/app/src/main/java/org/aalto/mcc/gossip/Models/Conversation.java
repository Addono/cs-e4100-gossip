package org.aalto.mcc.gossip.Models;

import android.graphics.Bitmap;

import java.util.Date;

public class Conversation {

    // user profile picture
    private Bitmap profile;

    // group name
    private String name;

    // received time
    private String time;

    // message content
    private String content;

    // number of unread messages
    private int unreadNum;

    // group ID
    private String groupID;

    public Conversation () {

    }

    public Conversation (Bitmap profile, String name, String time, String content, int unreadNum, String groupID) {
        this.profile = profile;
        this.name = name;
        this.time = time;
        this.content = content;
        this.unreadNum = unreadNum;
        this.groupID = groupID;
    }

    public Bitmap getProfile() {
        return profile;
    }

    public void setProfile(Bitmap profile) {
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUnreadNum() {
        return unreadNum;
    }

    public void setUnreadNum(int unreadNum) {
        this.unreadNum = unreadNum;
    }

    public String getGroupID(){
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }
}
