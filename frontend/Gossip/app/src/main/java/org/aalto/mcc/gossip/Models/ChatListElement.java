package org.aalto.mcc.gossip.Models;

public class ChatListElement {
    private final String groupName;
    private final ChatLastMessage chatLastMessage;
    private final String groupId;

    public ChatListElement(String groupName, ChatLastMessage chatLastMessage, String groupId) {
        this.groupName = groupName;
        this.chatLastMessage = chatLastMessage;
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public ChatLastMessage getChatLastMessage() {
        return chatLastMessage;
    }

    public String getGroupId() {
        return groupId;
    }

    @Override
    public String toString() {
        return "groupName: " + groupName + " lastBody: " + chatLastMessage.getLastMessage();
    }
}
