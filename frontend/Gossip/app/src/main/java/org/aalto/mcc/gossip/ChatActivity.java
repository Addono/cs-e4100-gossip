package org.aalto.mcc.gossip;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.DateFormat;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import org.aalto.mcc.gossip.Adapters.ChatItemAdapter;
import org.aalto.mcc.gossip.Adapters.MenuAdapter;
import org.aalto.mcc.gossip.Models.Conversation;
import org.aalto.mcc.gossip.Models.MenuContent;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class ChatActivity extends AppCompatActivity {

    private ListView listview;

    private ListView leftlistview;

    private List<Conversation> ConversationList;

    private ChatItemAdapter adapter;

    private Bitmap user_icon;

    private MenuAdapter menuAdapter;

    private ImageButton btn_add;

    private ImageButton btn_menu;

    private DrawerLayout drawerLayout;

    private List<MenuContent> list;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        listview = (ListView) findViewById(R.id.chat_list);
        btn_add = (ImageButton) findViewById(R.id.imgbtn_cl_add);
        leftlistview = (ListView) findViewById(R.id.left_listview);
        btn_menu = (ImageButton) findViewById(R.id.imgbtn_cl_menu);
        drawerLayout = (DrawerLayout) findViewById(R.id.chatlist_top);

        user_icon = BitmapFactory.decodeResource(this.getResources(), R.drawable.user_avatar);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Switch to the group chat view when an item is clicked
                Conversation clickedItem = (Conversation) listview.getAdapter().getItem(position);

                Intent intent = new Intent(ChatActivity.this, MessagesListActivity.class);
                intent.putExtra("groupID", clickedItem.getGroupID());
                startActivity(intent);
            }
        });

        String userUid = MainActivity.getFirebaseAuth().getCurrentUser().getUid();
        Query userToGroupQuery = FirebaseFirestore.getInstance()
                .collection("user_to_group")
                .document(userUid)
                .collection("groups")
                .orderBy("last_message_time_stamp", Query.Direction.DESCENDING);

        // Initialize the chat list
        userToGroupQuery
                .get()
                .addOnSuccessListener(this::generateChatList)
                .addOnFailureListener(err -> Log.e("ChatActivity", err.getMessage(), err));

        // Create a listener for the list of groups the user is in
        userToGroupQuery
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("@@@", "Listen failed.", e);
                            return;
                        }

                        generateChatList(snapshots);
                    }
                });
        // set listener to add user button
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChatActivity.this, AddGroupActivity.class);
                startActivity(intent);
            }
        });
        // Initialize left drawer menu
        InitMenu();
        menuAdapter = new MenuAdapter(this, list);
        leftlistview.setAdapter(menuAdapter);
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        // set listener to menu items
        leftlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    drawerLayout.closeDrawer(Gravity.START);
                    Intent intent1 = new Intent(ChatActivity.this, ProfileActivity.class);
                    startActivity(intent1);
                }
                if (i == 1) {
                    drawerLayout.closeDrawer(Gravity.START);
                    Intent intent2 = new Intent(ChatActivity.this, SettingsActivity.class);
                    startActivity(intent2);
                }
            }
        });

    }

    private void InitMenu() {
        list = new ArrayList<>();

        list.add(new MenuContent(R.drawable.profile_icon, "Profile", 1));
        list.add(new MenuContent(R.drawable.settings, "Settings", 2));
    }

    private void generateChatList(QuerySnapshot snapshots) {
        List<Conversation> data = new ArrayList<>();
        for (DocumentSnapshot doc : snapshots.getDocuments()) {
            try {
                data.add(new Conversation(
                        user_icon,
                        doc.getString("group_name"),
                        DateFormat.getDateInstance(DateFormat.SHORT).format(doc.getDate("last_message_time_stamp")),
                        doc.getString("last_message_author_display_name") + ": " + doc.getString("last_message"),
                        Math.toIntExact(doc.getLong("unread_count")),
                        doc.getId()
                ));
            } catch (NullPointerException exception) {
                Log.e("@@@", "Cought null pointer when updating group list, most likely are not all group fields present", exception);
            }
        }

        listview.setAdapter(new ChatItemAdapter(ChatActivity.this, data));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Toast.makeText(ChatActivity.this, "Return Disabled", Toast.LENGTH_SHORT).show();
            if( drawerLayout.isDrawerOpen(GravityCompat.START))
            {
                drawerLayout.closeDrawer(Gravity.START);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
