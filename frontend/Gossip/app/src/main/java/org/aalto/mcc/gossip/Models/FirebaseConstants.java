package org.aalto.mcc.gossip.Models;

import android.content.Context;
import android.util.Log;

public class FirebaseConstants {
    public final static String PROFILE_IMAGES_PATH = "profile_pictures/";
    public final static String PROFILE_IMAGE_SUFFIX = "/profile_picture.jpg";
    public final static String BASE_PATH = "gs://mcc-fall-2018-g03.appspot.com/";
    public final static String MESSAGES_PREFIX = "messages/";
    public final static String CHAT_IMAGES_PREFIX = "groups/";
    public final static String CHAT_IMAGES_HIGH_QULAITY_SUFFIX = "/high/high_image.jpg";
    public final static String CHAT_IMAGES_LOW_QULAITY_SUFFIX = "/low/low_image.jpg";
    public final static String CHAT_IMAGES_ORIGINAL_QULAITY_SUFFIX = "/image.jpg";

    public static String getUserPhoto(String userID){
        return BASE_PATH+PROFILE_IMAGES_PATH + userID + PROFILE_IMAGE_SUFFIX;
    }

    public static String getChatImagesPath(Context context, String groupID, String imageID) {
        String quality = context.getSharedPreferences("res", Context.MODE_PRIVATE).getString("resolution", "full");
        String base = BASE_PATH+CHAT_IMAGES_PREFIX + groupID + "/"+ MESSAGES_PREFIX + imageID;
        switch (quality) {
            case "low":
                return base + CHAT_IMAGES_LOW_QULAITY_SUFFIX;
            case "high":
                return base + CHAT_IMAGES_HIGH_QULAITY_SUFFIX;
            case "full":
                return base + CHAT_IMAGES_ORIGINAL_QULAITY_SUFFIX;
            default:
                Log.e("settings", "Illegal setting value found: " + quality + ", either low, high or full expected");
                return "";
        }
    }
}
