package org.aalto.mcc.gossip;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.aalto.mcc.gossip.util.Backend;
import org.aalto.mcc.gossip.util.GlideApp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class ProfileActivity extends AppCompatActivity {

    private EditText etxt_username;

    private TextView txt_email;

    private ImageView img_profile;

    private FirebaseAuth auth;

    private Uri imageUri;

    private ArrayList<String> nameList;

    private FirebaseUser user;

    private String displayName;

    private boolean loadedName = false;

    private boolean backPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        etxt_username = (EditText) findViewById(R.id.etxt_pf_username);
        txt_email = (TextView) findViewById(R.id.txt_pf_email);
        img_profile = (ImageView) findViewById(R.id.img_pf_photo);

        auth = MainActivity.getFirebaseAuth();
        user = auth.getCurrentUser();

        // get display name by userID
        Backend.getUserDetailById(user.getUid()).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    displayName = task.getResult().getString("display_name");
                    etxt_username.setText(displayName);
                }
            }
        });

        nameList = new ArrayList<String>();

        Backend.getAllUsers().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for(QueryDocumentSnapshot each: task.getResult()){
                        nameList.add(each.getString("display_name"));
                        Log.d("USER", each.getString("display_name"));
                    }
                    loadedName = true;
                }
                else {
                    Log.d("USER","failed", task.getException());
                }
            }
        });

        //img_profile.setImageDrawable(overlay(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.user_avatar)));

        // clear focus
        etxt_username.clearFocus();
        closeInputMethod();

        // set on focus listener to edittext username
        etxt_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    // tmp stores the input display name
                    String tmp = ((EditText)v).getText().toString();


                    // check if the edittext is empty
                    if(TextUtils.isEmpty(tmp.trim()) || !CheckUniqueName(tmp) || !loadedName || backPressed){
                        //set the display name before change
                        Backend.getUserDetailById(user.getUid()).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if(task.isSuccessful()){
                                    String name = task.getResult().getString("display_name");
                                    etxt_username.setText(name);
                                    Toast.makeText(ProfileActivity.this,"Invalid display name",Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                    }
                    else {
                        Backend.updateDisplayName(tmp)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Log.d("User", "Profile updated");
                                            Toast.makeText(ProfileActivity.this,"Display name changed successfully", Toast.LENGTH_LONG).show();
                                        }
                                        else
                                            Log.e("User", "failed to change the name", task.getException());
                                    }
                                });
                    }

                }
            }
        });

        setActionBar();

        img_profile.setImageDrawable(overlay(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.user_avatar)));

        // get username and email, set text here
        txt_email.setText(user.getEmail());

        // Load the profile picture from Firebase Storage and show it to the user
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String profilePictureLocation = "profile_pictures/" + userId + "/profile_picture.jpg";

        StorageReference profilePicture = FirebaseStorage.getInstance().getReference(profilePictureLocation);
        GlideApp.with(ProfileActivity.this)
                .load(profilePicture)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(img_profile);

        // set listener to profile image
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchChoosePictureOrTakePictureActivity();
            }
        });
    }

    private boolean CheckUniqueName(String newname){

        boolean unique = true;

        for( int i = 0; i < nameList.size(); i++){
            if( newname.equals(nameList.get(i))){
                unique = false;
                break;
            }
        }

        return unique;
    }

    // set actionbar and add a return button to it
    private void setActionBar() {
        ActionBar actionBar = getSupportActionBar();
        // display goback button
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setTitle("Profile");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // initially close input method
    private void closeInputMethod() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean isOpen = imm.isActive();
        if (isOpen) {
            imm.hideSoftInputFromWindow(etxt_username.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    //  clear focus if click blank space
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }

        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};

            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {

                return false;
            } else {

                v.setFocusable(false);

                v.setFocusableInTouchMode(true);
                return true;
            }
        }
        return false;
    }

    private void launchChoosePictureOrTakePictureActivity(){
        Intent pickIntent = new Intent();
        pickIntent.setType("image/*");
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File imageFile = Utils.createImageFile(ProfileActivity.this);
        imageUri = FileProvider.getUriForFile(ProfileActivity.this,BuildConfig.APPLICATION_ID+".provider", imageFile);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        String pickTitle = "Select or take a new Picture";
        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,new Intent[] { takePhotoIntent, new Intent(ProfileActivity.this, DeleteProfileImageActivity.class)});

        startActivityForResult(chooserIntent, Requests.CHOOSE_IMAGE);
    }

    private Drawable overlay(Drawable drawable) {
        Drawable[] layers = new Drawable[2];
        layers[0] =  drawable;
        layers[1] = ContextCompat.getDrawable(ProfileActivity.this, R.drawable.transparent_choose_photo);
        return new LayerDrawable(layers);
    }

    private void processChooseImageResult(int resultCode, Intent data){
        if(resultCode == Activity.RESULT_OK){
            if(data != null && data.hasExtra("remove_image")){
                img_profile.setImageDrawable(overlay(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.user_avatar)));
                imageUri = null;
                return;
            }
            boolean isCamera;
            if (data == null) {
                isCamera = true;
            } else {
                String action = data.getAction();
                if (action == null) {
                    isCamera = false;
                } else {
                    isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                }
            }

            if (!isCamera) {
                imageUri = data.getData();
            }

//            Drawable profilePhoto = new BitmapDrawable(getResources(), imageUri.get);
//            img_profile.setImageDrawable(overlay(Drawable.createFromPath(imageUri.getPath())));

            // Resize the image
            RequestCreator requestCreator = Picasso.get()
                    .load(imageUri)
                    .resize(img_profile.getWidth(), img_profile.getHeight())
                    .centerCrop();

            requestCreator.into(img_profile);

            // Resize and upload the image
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Bitmap resizedPhoto = requestCreator.get();

                        // Upload the image
                        Backend.updateProfilePicture(resizedPhoto).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Toast.makeText(ProfileActivity.this, "Profile picture uploaded successfully", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e("profile", "Failed to upload the profile picture");
                                Toast.makeText(ProfileActivity.this, "Failed to add profile picture", Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (IOException e) {
                        Log.e("profile", "Couldn't get the file for the new profile picture", e);
                    }
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode){
            case Requests.CHOOSE_IMAGE:{
                processChooseImageResult(resultCode, data);
            }   break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Toast.makeText(ChatActivity.this, "Return Disabled", Toast.LENGTH_SHORT).show();
            //return true;
            backPressed = true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
