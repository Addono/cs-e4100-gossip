package org.aalto.mcc.gossip;

import android.bluetooth.le.AdvertiseData;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.aalto.mcc.gossip.Adapters.AddedUserAdapter;
import org.aalto.mcc.gossip.Models.AddedUser;
import org.aalto.mcc.gossip.util.Backend;

import java.util.ArrayList;
import java.util.List;

public class AddGroupActivity extends AppCompatActivity {

    private AddedUserAdapter adapter;

    private ListView listview;

    private ImageButton btn_add;

    private ImageButton btn_back;

    private EditText etxt_search;

    private List<AddedUser> userList;

    private FirebaseAuth auth;

    private FirebaseUser user;

    private String groupID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);

        listview = (ListView) findViewById(R.id.added_list);
        btn_add = (ImageButton) findViewById(R.id.imgbtn_ag_add);
        btn_back = (ImageButton) findViewById(R.id.imgbtn_ag_back);
        etxt_search = (EditText) findViewById(R.id.etxt_search);

        Intent it = getIntent();
        if(it != null){
            groupID = it.getStringExtra("groupID");
        }
        /*
        * see Class AddedUser for user data structure
        * note that the fetch function should return a list of AddedUser*/
        //userList = initDemoData();

        auth = MainActivity.getFirebaseAuth();
        FirebaseUser user = auth.getCurrentUser();

        final String userid = user.getUid();
        //Log.d("@@@",userid);

        userList = new ArrayList<AddedUser>();

        adapter = new AddedUserAdapter(this);
        Backend.getAllUsers().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for(QueryDocumentSnapshot each: task.getResult()){
                        if( !each.getId().equals(userid) )
                        {
                            userList.add(new AddedUser(each.getString("display_name"), each.getId()));
                            //Log.d("@@@","add user "+each.getId());
                        }
                        //Log.d("USER", each.getString("display_name"));
                    }
                    adapter.setData(userList);
                }
                else {
                    Log.d("USER","failed", task.getException());
                }
            }
        });
        // initiate and set ListView adapter
        //adapter = new AddedUserAdapter(this);
        //adapter.setData(userList);

        // initiate and set ListView adapter



        intiEditView();

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final List<AddedUser> checked = adapter.checkedUser();
                //this line is for debugging
                if(checked != null && groupID == null){
                    // create group
                    Toast.makeText(AddGroupActivity.this, "Please wait a few seconds;)", Toast.LENGTH_LONG).show();
                    final Pair<Task<Void>, DocumentReference> newGroup = Backend.createGroup();
                    newGroup.first.addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                           if(task.isSuccessful()){
                               final String newGroupId = newGroup.second.getId();
                               Log.d("Group", "Succeed create group, id: "+ newGroupId);
                               for(int i = 0 ; i < checked.size(); i++){
                                   //  add all selected user to group
                                   //Toast.makeText(AddGroupActivity.this,"position "+checked.get(i).getUsername(),Toast.LENGTH_SHORT).show();
                                   Backend.addUserToGroup(checked.get(i).getUserid(), newGroupId)
                                           .addOnCompleteListener(new OnCompleteListener<Void>() {
                                               @Override
                                               public void onComplete(@NonNull Task<Void> task) {
                                                   if(task.isSuccessful()){
                                                       Log.d("Group", "Succeed add user to group, id: "+ newGroupId);
                                                   }else{
                                                       Log.d("Group","failed", task.getException());
                                                   }
                                               }
                                           });
                               }
                               // Go to chat
                               Intent intent = new Intent(AddGroupActivity.this, MessagesListActivity.class);
                               intent.putExtra("groupID", newGroupId);
                               startActivity(intent);
                           }else{
                               Log.d("Group","failed", task.getException());
                           }
                        }
                    });
                }
                else if(checked != null && groupID != null){
                    for(int i = 0 ; i < checked.size(); i++){
                        //  add all selected user to group
                        Backend.addUserToGroup(checked.get(i).getUserid(), groupID)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Log.d("Group", "Succeed add user to group, id: "+ groupID);
                                        }else{
                                            Log.d("Group","failed", task.getException());
                                        }
                                    }
                                });
                    }
                    // Go to chat
                    Intent intent = new Intent(AddGroupActivity.this, MessagesListActivity.class);
                    intent.putExtra("groupID", groupID);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(AddGroupActivity.this, "Please select an user", Toast.LENGTH_LONG).show();
                }

            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddGroupActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });
    }

    private List<AddedUser> initDemoData() {

        List<AddedUser> list = new ArrayList<AddedUser>();
        list.add(new AddedUser("Alice","adsd"));
        list.add(new AddedUser("Alex","hsd"));
        list.add(new AddedUser("Bob","hs"));

        return list;
    }

    private void intiEditView() {
        etxt_search.addTextChangedListener(new TextWatcher() {

            private CharSequence tmp;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //filter user names
                tmp = s;
                adapter.setNoFilter(false);
                listview.setAdapter(adapter);
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                /*Toast.makeText(AddGroupActivity.this,
                        "input: "+ tmp.length(), Toast.LENGTH_SHORT)
                        .show();*/
                // if there's nothing in the search bar, stop displaying user names
                if(tmp.length() == 0){
                    adapter.setNoFilter(true);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }
}
