package org.aalto.mcc.gossip;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firestore.admin.v1beta1.Progress;

import org.aalto.mcc.gossip.Adapters.ChatImagesGridViewAdapter;
import org.aalto.mcc.gossip.Models.Message;
import org.aalto.mcc.gossip.util.Backend;

import java.util.ArrayList;
import java.util.List;

public class MessagesListMenuActivity extends AppCompatActivity {
    private String groupID;

    private List<String> images;

    private Button leaveGroupButton;
    private Button addMemberToGroupButton;

    private List<String> foodImages;
    private List<String> techImages;
    private List<String> screenshotImages;
    private List<String> otherImages;

    private GridView foodImagesGridView;
    private GridView techImagesGridView;
    private GridView screenshotImagesGridView;
    private GridView otherImagesGridView;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groupID = getIntent().getStringExtra("groupID");
        images = getIntent().getStringArrayListExtra("images");
        setContentView(R.layout.activity_messages_list_menu);

        foodImagesGridView = findViewById(R.id.food_images);

        foodImages = new ArrayList<>();
        foodImages.add("gs://mcc-fall-2018-g03.appspot.com/groups/QDQSbY46NawS9uETUf9c/messages/iepyV7DTnzsVSScTqvHK/image.png");
        foodImages.add("gs://mcc-fall-2018-g03.appspot.com/groups/QDQSbY46NawS9uETUf9c/messages/iepyV7DTnzsVSScTqvHK/image.png");
        foodImages.add("gs://mcc-fall-2018-g03.appspot.com/groups/QDQSbY46NawS9uETUf9c/messages/iepyV7DTnzsVSScTqvHK/image.png");
        foodImages.add("gs://mcc-fall-2018-g03.appspot.com/groups/QDQSbY46NawS9uETUf9c/messages/iepyV7DTnzsVSScTqvHK/image.png");
        setAdpater(foodImagesGridView);

        techImagesGridView = findViewById(R.id.tech_images);
        setAdpater(techImagesGridView);

        screenshotImagesGridView = findViewById(R.id.screenshot_images);
        setAdpater(screenshotImagesGridView);

        otherImagesGridView = findViewById(R.id.other_images);
        setAdpater(otherImagesGridView);

        leaveGroupButton = findViewById(R.id.btn_leaveGroup);
        addMemberToGroupButton = findViewById(R.id.btn_addMember);

        progressDialog = new ProgressDialog(this);

        addListeners();
    }

    private void addListeners(){
        leaveGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setMessage("Leaving the group...");
                Backend.leaveChat(groupID).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressDialog.dismiss();
                        Toast.makeText(MessagesListMenuActivity.this,"Left the group",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(MessagesListMenuActivity.this,"Opps! something goes wrong, try later",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        addMemberToGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MessagesListMenuActivity.this,"not yet",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void groupImages(){
        for(String url:images){
            // how to determine group of an image here ?
        }
    }

    public void setGridViewHeight(GridView gridview) {

        ListAdapter listAdapter = gridview.getAdapter();
        if (listAdapter == null) {
            return;
        }

        gridview.setColumnWidth(150);
        int item_num = listAdapter.getCount();
        int numColumns = item_num / 3 + item_num % 3;

        int totalHeight = 0;

        for (int i = 0; i < listAdapter.getCount(); i += numColumns) {
            View listItem = listAdapter.getView(i, null, gridview);
            listItem.measure(0, 0);
            // get total height
            totalHeight += listItem.getMeasuredHeight();
        }

        // get gridview parameter
        ViewGroup.LayoutParams params = gridview.getLayoutParams();
        // set height
        params.height = totalHeight;
        // set parameter
        gridview.setLayoutParams(params);
    }

    public void setAdpater(GridView view){

        ChatImagesGridViewAdapter adapter = new ChatImagesGridViewAdapter(this,foodImages);
        view.setAdapter(adapter);
        setGridViewHeight(view);
        adapter.notifyDataSetChanged();

    }

}
