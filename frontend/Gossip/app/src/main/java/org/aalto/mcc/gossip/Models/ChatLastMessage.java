package org.aalto.mcc.gossip.Models;

import org.joda.time.DateTime;

public class ChatLastMessage {

    private final String lastMessage;
    private final DateTime lastMessageDateTime;
    private final MessageType messageType;

    public ChatLastMessage(String lastMessage, DateTime lastMessageDateTime, MessageType messageType) {
        this.lastMessage = lastMessage;
        this.lastMessageDateTime = lastMessageDateTime;
        this.messageType = messageType;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public DateTime getLastMessageDateTime() {
        return lastMessageDateTime;
    }

    public MessageType getMessageType() {
        return messageType;
    }
}
