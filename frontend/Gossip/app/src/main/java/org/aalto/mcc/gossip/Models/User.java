package org.aalto.mcc.gossip.Models;

import com.stfalcon.chatkit.commons.models.IUser;

public class User implements IUser {

    private String id;
    private String name;
    private boolean online;

    public User(String id, String name, boolean online) {
        this.id = id;
        this.name = name;
        this.online = online;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatar() {
        return FirebaseConstants.getUserPhoto(id);
    }

    public boolean isOnline() {
        return online;
    }
}