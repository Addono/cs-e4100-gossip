package org.aalto.mcc.gossip;

public class Requests {
    public static final int CHOOSE_IMAGE = 100;
    public static final int LOGIN = 101;
    public static final int SIGNUP = 102;
    public static final int REQUEST_EXIT= 103;
}
