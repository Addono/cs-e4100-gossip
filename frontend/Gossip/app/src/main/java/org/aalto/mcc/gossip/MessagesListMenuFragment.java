package org.aalto.mcc.gossip;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;

import org.aalto.mcc.gossip.util.Backend;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesListMenuFragment extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private List<String> images = new ArrayList<String>();
    //The original data structure is too complicate to construct and update, divide it to three map
    private Map<String, List<String>> authorCategory = new HashMap<>();
    private Map<String, List<String>> dateCategory = new HashMap<>();
    private Map<String, List<String>> labelCategory = new HashMap<>();

    private String groupID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_list_menu_fragment);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.goback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle("Gallery");

        images = getIntent().getStringArrayListExtra("images");

        groupID = getIntent().getStringExtra("groupID");

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        categoryImage(images);

        //findViewById(R.id.imgbtn_)
    }

    private void categoryImage(List<String> images){
        for (String eachImg: images) {
            Backend.getMessageInfo(eachImg.split("/")[4], eachImg.split("/")[6])
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if(task.isSuccessful()){
                                String author = task.getResult().getString("author");
                                Timestamp created = task.getResult().getTimestamp("created");
                                if(daysPassed(created.toDate()) < 1)
                                    dateCategory.computeIfAbsent("today", k -> new ArrayList<>()).add(eachImg);
                                else if (daysPassed(created.toDate()) == 1)
                                    dateCategory.computeIfAbsent("yesterday", k -> new ArrayList<>()).add(eachImg);
                                else if (daysPassed(created.toDate()) < 7)
                                    dateCategory.computeIfAbsent("this week", k -> new ArrayList<>()).add(eachImg);
                                else
                                    dateCategory.computeIfAbsent("older", k -> new ArrayList<>()).add(eachImg);
                                String category = task.getResult().getString("feature");
                                labelCategory.computeIfAbsent(category, k -> new ArrayList<>()).add(eachImg);
                                authorCategory.computeIfAbsent(author, k-> new ArrayList<>()).add(eachImg);
//                                Backend.getUserDetailById(author).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//                                    @Override
//                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                                        authorCategory.computeIfAbsent(task.getResult().getString("display_name"), k -> new ArrayList<>()).add(eachImg);
//                                    }
//                                });
                            }
                        }
                    });

        }
    }

    private static int daysPassed(Date date)
    {
        return (int) ((Timestamp.now().toDate().getTime() - date.getTime()) / (1000*3600*24));
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putSerializable("images", (Serializable) dateCategory);
        ShowImagesByDate showImagesByDate = new ShowImagesByDate();
        showImagesByDate.setArguments(bundle);
        adapter.addFragment(showImagesByDate, "By Date");

        bundle = new Bundle();
        bundle.putSerializable("images", (Serializable) authorCategory);
        ShowImagesByAuthor showImagesByAuthor = new ShowImagesByAuthor();
        showImagesByAuthor.setArguments(bundle);
        adapter.addFragment(showImagesByAuthor, "By Author");

        bundle = new Bundle();
        bundle.putSerializable("images", (Serializable) labelCategory);
        ShowImagesByGroup showImagesByGroup = new ShowImagesByGroup();
        showImagesByGroup.setArguments(bundle);
        adapter.addFragment(showImagesByGroup, "By Category");

        ChatSettingsFragment chatSettingsFragment =  new ChatSettingsFragment();
        bundle = new Bundle();
        bundle.putString("groupID", groupID);
        chatSettingsFragment.setArguments(bundle);
        adapter.addFragment(chatSettingsFragment, "Settings");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
