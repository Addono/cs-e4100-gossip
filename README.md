# Gossip
The app can be deployed by using the `deploy.sh` script in a terminal positioned at the root of the project.

## Functionality
Run the `deploy.sh` script without any options to deploy both the front and backend. For the frontend does this mean that the app is build and pushed onto ADB for testing. The backend deploys the cloud functions and access rules to the Firestore project.

### Help
The help of the deploy script can be accessed by passing the `-h` option.

### Deploy either the front or backend
By using the `--only` flag with either `backend` or `frontend` as value will only the respective part be deployed. If it is not set, then both the frontend and the backend are deployed.


## Requirements
The project assumes that the following requirements are satisfied:
 * Firebase CLI is installed and the user is logged into - and selected - the mcc-fall-2018-g03 project with owner rights.
 * NPM is installed.
 * ABD is installed.