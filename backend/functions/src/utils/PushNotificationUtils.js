const admin = require('firebase-admin');
const _ = require('lodash');

const Priority = require('../enums/Priority');
const MessageType = require('../enums/MessageType');
const PushNotificationType = require('../enums/PushNotificationType');
const FirestoreUtils = require('./FirestoreUtils');

const ttl = 24 * 3600 * 1000; //24 hours in miliseconds
const debug = false;

const sendNotification = (userId, deviceToken, groupId, dataParams = {}, androidParams = {}) => {
    const androidSettings = {
        ttl,
        priority: Priority.high,
        ...androidParams
    };

    const message = {
        android: androidSettings,
        data: {
            groupId,
            ...dataParams
        },
        token: deviceToken
    };

    if (debug) { //Debugging
        console.log(`Sending push notification to device: 
        deviceToken: ${deviceToken}, 
        groupId: ${groupId}, 
        dataParams: ${JSON.stringify(dataParams, null, 2)}
        androidParams: ${JSON.stringify(androidParams, null, 2)}
        `);
    }

    return admin.messaging().send(message) //add message, true for dryRun
        .then((response) => {
            console.log('Successfully sent message: ', response);
            return Promise.resolve(response);
        })
        .catch((err) => {
            return handleFcmError(err, userId, deviceToken)
        });
};

const sendNewTextMessageNotification = (userId, deviceToken, groupId, groupName, authorDisplayName, body) => {
    if (debug) {
        console.log(`sendNewTextMessageNotification ${deviceToken}; ${groupId}; ${groupName}; ${authorDisplayName}; ${body}`);
    }

    // Only supported form Oreo, not a target device in this project
    // const androidParams = {
    //     collapseKey: CollapseKey.group_message
    // };

    const dataParams = {
        type: MessageType.text,
        title: groupName,
        body: `${authorDisplayName}: ${body}`,
        push_notification_type: PushNotificationType.new_message
    };

    return sendNotification(userId, deviceToken, groupId, dataParams);
};

const sendNewImageMessageNotification = (userId, deviceToken, groupId, groupName, authorDisplayName, imagePath) => {
    if (debug) {
        console.log(`sendNewImageMessageNotification ${deviceToken}; ${groupId}; ${groupName}; ${authorDisplayName}; ${imagePath}`);
    }

    // Only supported form Oreo, not a target device in this project
    // const androidParams = {
    //     collapseKey: CollapseKey.group_message
    // };

    const dataParams = {
        type: MessageType.image,
        title: groupName,
        body: `${authorDisplayName} has sent a new image.`,
        push_notification_type: PushNotificationType.new_message,
        imagePath
    };

    return sendNotification(userId, deviceToken, groupId, dataParams);
};

const sendAddedToGroupNotification = (userId, deviceToken, groupId, groupName) => {
    if (debug) {
        console.log(`sendAddedToGroupNotification ${deviceToken}; ${groupId}; ${groupName}`);
    }

    // Only supported form Oreo, not a target device in this project
    // const androidParams = {
    //     collapseKey: CollapseKey.added_to_group_message
    // };

    const dataParams = {
        type: MessageType.text,
        title: groupName,
        body: `You have been added to ${groupName}.`,
        push_notification_type: PushNotificationType.added_to_group
    };

    return sendNotification(userId, deviceToken, groupId, dataParams);
};

const handleFcmError = (err, userId, deviceToken) => {
    switch (err.code) {
        case 'messaging/registration-token-not-registered':
            console.log(err);
            return FirestoreUtils.removeInvalidDeviceToken(userId, deviceToken);
        //Ideally, we would implement other error response handles from FCM, however many would require exponential
        // back off, which is really not a good idea on firebase functions, as it costs money.
        default:
            console.error(err);
            return Promise.reject(err);
    }
};

class PushNotificationUtils {

    static async sendAddedToGroupNotification(snapshot, context) {
        const groupId = context.params.groupId;
        const userId = context.params.userId;
        return Promise.all([
            FirestoreUtils.getGroupName(groupId),
            FirestoreUtils.getDeviceTokensByUserId(userId)
        ]).then(([groupName, deviceTokens]) => {
            if (debug) {
                console.log(`addedToGroupNotification groupName ${groupName}, deviceTokens: ${deviceTokens}`)
            }
            return Promise.all([...deviceTokens.map((deviceToken) => sendAddedToGroupNotification(userId, deviceToken, groupId, groupName))]);
        }).catch((err) => {
            console.error("Error sending addedToGroupPushNotification", JSON.stringify(err, null, 2));
            return Promise.reject(err);
        })
    }

    static async sendNewMessageNotification(snapshot, context, isUpdate) {
        const groupId = context.params.groupId;
        const messageId = context.params.messageId;

        const data = isUpdate ? snapshot.after.data() : snapshot.data();

        const {author, type, body} = data;

        const groupMemberUserIds = await FirestoreUtils.getGroupMembersByGroupId(groupId);
        const authorRemovedGroupMemberUserIds = groupMemberUserIds.filter(id => id !== author);
        const deviceTokens = await FirestoreUtils.batchGetDeviceTokensByUserIds(authorRemovedGroupMemberUserIds);

        const deviceTokensByUsers = _.zipObject(authorRemovedGroupMemberUserIds, deviceTokens);

        return Promise.all([
            FirestoreUtils.getGroupName(groupId),
            FirestoreUtils.getDisplayNameByUserId(author)
        ]).then(([groupName, authorDisplayName]) => {

            switch (type) {
                case MessageType.text:
                    return Promise.all([...Object.keys(deviceTokensByUsers).map(userId => {
                        const userDeviceTokens = deviceTokensByUsers[userId];
                        return Promise.all([...userDeviceTokens
                            .map((deviceToken) =>
                                sendNewTextMessageNotification(userId, deviceToken, groupId, groupName, authorDisplayName, body)
                            )]);
                    })]);
                case MessageType.image:
                    const imagePath = data.low;
                    return Promise.all([...Object.keys(deviceTokensByUsers).map(userId => {
                        const userDeviceTokens = deviceTokensByUsers[userId];
                        return Promise.all([...userDeviceTokens
                            .map((deviceToken) =>
                                sendNewImageMessageNotification(userId, deviceToken, groupId, groupName, authorDisplayName, imagePath)
                            )]);
                    })]);
                default:
                    return Promise.resolve(`Not handled image type: ${type} for group ${groupId} with messageId: ${messageId}`);
            }
        }).catch((err) => {
            console.error("Error sending newMessageNotification", err);
            return Promise.reject(err);
        })
    }

}

module.exports = PushNotificationUtils;