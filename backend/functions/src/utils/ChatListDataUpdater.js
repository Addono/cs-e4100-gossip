const admin = require('firebase-admin');

const FirestoreUtils = require('./FirestoreUtils');
const MessageType = require('../enums/MessageType');

const getBodyByMessageType = (messageType, snapshot, authorDisplayName, messageId, hasMoreThanTwoMembers) => {
    switch (messageType) {
        case MessageType.text:
            return hasMoreThanTwoMembers ? `${authorDisplayName}: ${snapshot.data().body}` : snapshot.data().body;
        case MessageType.image:
            return `${authorDisplayName} sent a photo.`;
        case MessageType.image_pending:
            return `${authorDisplayName} is sending a photo.`;
        case MessageType.image_failure:
            return `Oh snap! ${authorDisplayName} tried to send a photo, but we messed up something. Our monkeys are working on it, we promise!`;
        default:
            throw new TypeError(`Unknown message type was set in the message with messageId: ${messageId}.`);
    }
};

class ChatListDataUpdater {

    static async cacheMessageOnUserAddedForChatList(snapshot, context) {
        const groupId = context.params.groupId;
        const newUserId = context.params.userId;

        const addedUserJoinedAtTimeStamp = snapshot.data().joined_on;

        return Promise.all([
            FirestoreUtils.getGroupName(groupId),
            FirestoreUtils.getDisplayNameByUserId(newUserId)
        ]).then(([groupName, addedUserDisplayName]) => {
            return FirestoreUtils.addUserAddedMessageAndUpdateUserToGroup(groupId, groupName, newUserId, addedUserDisplayName, addedUserJoinedAtTimeStamp);
        });
    }

    static cacheMessageOnUserLeftForChatList(snapshot, context) {
        const groupId = context.params.groupId;
        const leftUserId = context.params.userId;

        const userLeftAt = admin.firestore.FieldValue.serverTimestamp();

        return Promise.all([
            FirestoreUtils.getGroupName(groupId),
            FirestoreUtils.getDisplayNameByUserId(leftUserId)
        ]).then(([groupName, leftUserDisplayName]) => {
            return FirestoreUtils.addUserLeftMessage(groupId, groupName, leftUserId, leftUserDisplayName, userLeftAt)
        });
    }

    static async cacheMessageOnNewMessageForChatList(snapshot, context, isUpdate) {
        const groupId = context.params.groupId;
        const messageId = context.params.messageId;

        const data = isUpdate ? snapshot.after.data() : snapshot.data();

        const {author, type, created} = data;

        return Promise.all([
            FirestoreUtils.getGroupMembersByGroupId(groupId),
            FirestoreUtils.getGroupName(groupId),
            FirestoreUtils.getDisplayNameByUserId(author)
        ]).then(([groupMembers, groupName, authorDisplayName]) =>
            Promise.all([...groupMembers.map(groupMember => {
                const hasMoreThanTwoMembers = groupMembers.length > 2;
                const body = getBodyByMessageType(type, snapshot, authorDisplayName, messageId, hasMoreThanTwoMembers);
                return FirestoreUtils.updateUserToGroup(groupMember, groupId, groupName, body, author, authorDisplayName, created);
            })])
        );
    }
}

module.exports = ChatListDataUpdater;