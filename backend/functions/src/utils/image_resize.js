const admin = require('firebase-admin');
const path = require('path');
const os = require('os');
const fs = require('fs');
const sharp = require('sharp');
const db = admin.firestore();
const lowPrefix = 'low_';
const hiPrefix = 'high_';
const profilePath = 'profile_pictures';
const hiPath = 'high';
const lowPath = 'low';
const uuid = require("uuid/v4");

module.exports = async object => {
  const filePath = object.name;
  const fileDir = path.dirname(filePath);
  const hiFileDir = path.join(fileDir, hiPath);
  const lowFileDir = path.join(fileDir, lowPath);
  console.log('dictionary of high res file is', hiFileDir);
  console.log('dictionary of low res file is', lowFileDir);
  const fileName = path.basename(filePath);
  const tempFilePath = path.join(os.tmpdir(), uuid(), fileName);
  const tempFileDir = path.dirname(tempFilePath);
  const lowResFileName = `${lowPrefix}${fileName}`;
  const highResFileName = `${hiPrefix}${fileName}`;
  const lowResFilePath = path.join(lowFileDir, lowResFileName);
  const highResFilePath = path.join(hiFileDir, highResFileName);

  if (!object.contentType.startsWith('image/')) {
    console.log('This is not an image.');
    return null;
  }

  if (fileDir.endsWith(lowPath)) {
    console.log('It is a low res photo, no need to resize');
    return;
  }
  if (fileDir.endsWith(hiPath)) {
    console.log('It is a high res photo, no need to resize');
    return;
  }
  if (fileDir.startsWith(profilePath)) {
    console.log('It is a profile photo, no need to resize');
    return;
  }
  console.log(filePath);
  console.log(path.dirname(filePath));
  const imageRef = db.doc(fileDir);
  await imageRef.update({
    original: filePath
  });
  const bucket = admin.storage().bucket(object.bucket);

  fs.mkdirSync(tempFileDir);
  console.log(tempFilePath);
  await bucket.file(filePath).download({ destination: tempFilePath });
  console.log('The file has been downloaded to', tempFilePath);

  //see if is landscape
  const data = await sharp(tempFilePath).metadata();
  if (data.width > data.height) {
    await sharp(tempFilePath)
      .resize(640, 480)
      .toFile(path.join(tempFileDir, lowResFileName));
    await sharp(tempFilePath)
      .resize(1280, 960)
      .toFile(path.join(tempFileDir, highResFileName));
  } else {
    await sharp(tempFilePath)
      .resize(480, 640)
      .toFile(path.join(tempFileDir, lowResFileName));
    await sharp(tempFilePath)
      .resize(960, 1280)
      .toFile(path.join(tempFileDir, highResFileName));
  }

  console.log('The file resized at', path.join(tempFileDir, lowResFileName));
  await bucket.upload(path.join(tempFileDir, lowResFileName), {
    destination: lowResFilePath
  });
  console.log('Low resolution image uploaded to Storage at', lowResFilePath);
  console.log('The file resized at', path.join(tempFileDir, highResFileName));
  await bucket.upload(path.join(tempFileDir, highResFileName), {
    destination: highResFilePath
  });
  console.log('High resolution image uploaded to Storage at', highResFilePath);
  fs.unlinkSync(tempFilePath);
  fs.unlinkSync(path.join(tempFileDir, highResFileName));
  fs.unlinkSync(path.join(tempFileDir, lowResFileName));
  await imageRef.update({
    low: lowResFilePath,
    high: highResFilePath,
    type: 'image'
  });
};
