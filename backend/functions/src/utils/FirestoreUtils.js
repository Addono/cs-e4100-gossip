const admin = require("firebase-admin");
const MessageType = require('../enums/MessageType');

const debug = false;

class FirestoreUtils {
    static getDeviceTokensByUserId(userId) {
        if (debug) {
            console.log(`GetDeviceTokensByUser ${userId}`)
        }
        return admin.firestore().collection('device_tokens').doc(userId).get()
            .then((doc) => {
                if (!doc.exists) {
                    return Promise.resolve([]);
                } else {
                    return doc.data().tokens;
                }
            });
    }

    static batchGetDeviceTokensByUserIds(userIdList) {
        return Promise.all([...userIdList.map(this.getDeviceTokensByUserId)]);
    }

    static async getGroupMembersByGroupId(groupId) {
        if (debug) {
            console.log(`getGroupMemberDisplayNamesByGroupId ${groupId}`);
        }

        const maybeMembers = await admin.firestore()
            .collection('groups')
            .doc(groupId)
            .collection('members')
            .get();

        if (debug) {
            console.log(`MaybeMembers ${JSON.stringify(maybeMembers, null, 2)}`);
            maybeMembers.forEach(doc => {
                console.log(doc.id, '=>', doc.data());
            });
        }

        const members = [];
        maybeMembers.forEach(maybeMember => members.push(maybeMember.id)); //map() is not defined -_-

        return members;
    }

    static async getGroupMemberDisplayNamesByGroupId(groupId) {
        const members = await this.getGroupMembersByGroupId(groupId);

        if (debug) {
            console.log(`Members ${JSON.stringify(members, null, 2)}`);
        }
        return Promise.all([...members.map(FirestoreUtils.getDisplayNameByUserId)])

    }

    static getDisplayNameByUserId(userId) {
        if (debug) {
            console.log(`getDisplayNameByUserId ${userId}`)
        }
        return admin.firestore().collection('user_profiles').doc(userId).get()
            .then((doc) => {
                if (!doc.exists) {
                    return Promise.resolve("");
                } else {
                    return doc.data().display_name;
                }
            });
    }

    static async getGroupName(groupId) {
        if (debug) {
            console.log(`getGroupName ${groupId}`)
        }
        const groupMemberNames = await FirestoreUtils.getGroupMemberDisplayNamesByGroupId(groupId);
        return groupMemberNames.join(", ");
    }

    static removeInvalidDeviceToken(userId, deviceTokenToRemove) {
        const db = admin.firestore();
        return db.collection('device_tokens').doc(userId)
            .update({
                tokens: admin.firestore.FieldValue.arrayRemove(deviceTokenToRemove)
            });
    }

    static updateUserToGroup(userId, groupId, groupName, lastMessageBody, lastMessageAuthorId, lastMessageAuthorDisplayName, lastMessageTimeStamp) {
        const db = admin.firestore();
        const userToGroupGroupRef = db.collection('user_to_group').doc(userId).collection('groups').doc(groupId);
        return db.runTransaction(t => {
           return t.get(userToGroupGroupRef)
               .then(doc => {
                   const unreadCount = doc.data().unread_count + 1 || 1;

                   const updateObject = {
                       group_name: groupName,
                       last_message: lastMessageBody,
                       last_message_author_display_name: lastMessageAuthorDisplayName,
                       last_message_time_stamp: lastMessageTimeStamp,
                       author_id: lastMessageAuthorId,
                       unread_count: unreadCount
                   };

                   t.update(userToGroupGroupRef, updateObject);
               })
        });
    }

    static addUserAddedMessageAndUpdateUserToGroup(groupId, groupName, newUserId, newUserDisplayName, newUserJoinedTimeStamp) {
        const db = admin.firestore();
        const userToGroupGroupRef = db.collection('user_to_group').doc(newUserId).collection('groups').doc(groupId);
        const groupMessagesRef = db.collection('groups').doc(groupId).collection('messages');
        const newGroupMessageId = groupMessagesRef.doc().id;
        const batch = db.batch();

        const unreadCount = 0;
        const newUserMessageBody = `${newUserDisplayName} has joined the conversation.`;

        const newMessageObject = {
            author: newUserId,
            body: newUserMessageBody,
            created: newUserJoinedTimeStamp,
            type: MessageType.text
        };

        const userToGroupUpdateObject = {
            group_name: groupName,
            last_message: newUserMessageBody,
            last_message_author_display_name: newUserDisplayName,
            last_message_time_stamp: newUserJoinedTimeStamp,
            author_id: newUserId,
            unread_count: unreadCount
        };

        batch.set(groupMessagesRef.doc(newGroupMessageId), newMessageObject);
        batch.update(userToGroupGroupRef, userToGroupUpdateObject);
        return batch.commit();
    }

    static addUserLeftMessage(groupId, groupName, leftUserId, leftUserDisplayName, leftUserTimeStamp) {
        const db = admin.firestore();
        const groupMessagesRef = db.collection('groups').doc(groupId).collection('messages');

        const newUserMessageBody = `${leftUserDisplayName} has left the conversation.`;

        const newMessageObject = {
            author: leftUserId,
            body: newUserMessageBody,
            created: leftUserTimeStamp,
            type: MessageType.text
        };

        return groupMessagesRef.doc().set(newMessageObject);
    }
}

module.exports = FirestoreUtils;