const Priority = Object.freeze({
    normal: "normal",
    high: "high"
});

module.exports = Priority;