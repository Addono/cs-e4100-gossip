const PushNotificationType = Object.freeze({
    new_message: "new_message",
    added_to_group: "added_to_group"
});

module.exports = PushNotificationType;