const MessageType = Object.freeze({
    image: "image",
    text: "text",
    image_pending: "image_pending",
    image_failure: "image_failure"
});

module.exports = MessageType;