'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');

const serviceAccount = require('./service-account-credentials');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://mcc-fall-2018-g03.firebaseio.com"
});

const image_resize = require('./src/utils/image_resize');
const PushNotificationUtils = require('./src/utils/PushNotificationUtils');
const ChatListUpdater = require('./src/utils/ChatListDataUpdater');

//The file path should like group/${group_id}/messages/${message_id}/body/xxxx.xxx
exports.resizeImage = functions.storage.object().onFinalize(image_resize);


exports.handleAddedToGroup = functions.firestore
    .document('/groups/{groupId}/members/{userId}')
    .onCreate((snapshot, context) => Promise.all([
        PushNotificationUtils.sendAddedToGroupNotification(snapshot, context),
        ChatListUpdater.cacheMessageOnUserAddedForChatList(snapshot, context)
    ]));

exports.handleNewMessages = functions.firestore
    .document('/groups/{groupId}/messages/{messageId}')
    .onCreate((snapshot, context) => Promise.all([
        PushNotificationUtils.sendNewMessageNotification(snapshot, context, false),
        ChatListUpdater.cacheMessageOnNewMessageForChatList(snapshot, context, false)
    ]));

exports.handleMessageUpdates = functions.firestore
    .document('/groups/{groupId}/messages/{messageId}')
    .onUpdate((snapshot, context) => Promise.all([
        PushNotificationUtils.sendNewMessageNotification(snapshot, context, true),
        ChatListUpdater.cacheMessageOnNewMessageForChatList(snapshot, context, true)
    ]));

exports.handleLeftGroup = functions.firestore
    .document('/groups/{groupId}/members/{userId}')
    .onDelete((snapshot, context) => ChatListUpdater.cacheMessageOnUserLeftForChatList(snapshot, context));