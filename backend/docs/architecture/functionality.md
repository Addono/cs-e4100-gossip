# Functionality

Gives a high level overview of the functions supplied by the backend.

## Authentication & User Management

### User Login

### Signup User

### Update Profile Info

Profile picture, should we rescale this client or sever side?

(Optional) Other fields like gender, age, and so on.

## Chat management

### Create Chat

### User Search

Based on username, has to implement some constraints to prevent the query to return everyone, e.g. by requiring the query to have at least 5 characters.

### Add User to Chat

## Chatting

### Send Message

Both text and image based

### (optional) Edit Message

User should be:

* Author of the message
* Be able to access the message (joined before the message was send)

In practice does this mean that the user should be the author of the message and the message id should be higher than the message entry id set in the members list of the group.

### Retrieve Messages

## Image Processing

### Resize Images

### Feature Detection

## Image Retrieval

### Enforce Access Rules to Images

Not all users can view all images.

## Push Notifications

### Added to a Group

### Receive New Message

## Resource Usage Optimization

### (optional) Picture Expiration

### (optional) Remove Messages Which No One Can Access

Messages could become unviewable if all the present users joined later than the message was posted. These could be pruned to save resources.

### Leave Chat

Cleanup messages and stored images when last person leaves

## 