#!/usr/bin/env bash

PROJECT_ID="mcc-fall-2018-g03"
CURRENT_PATH=`pwd`

# Add colors to progress :)
green_echo() {
    prompt="$1"
    echo -e -n "\033[00;32m$prompt"
    echo -e "\033[0m"
}

# Define function to deploy firebase functions
deploy_backend () {
    green_echo "Step 1: Deploying backend."

    cd backend/functions

    green_echo "Step 1.1: cd into backend/functions and install node dependencies"
    rm -rf node_modules/
    npm install

    green_echo "Step 1.2: switch to group's firebase project"
    firebase use $PROJECT_ID

    green_echo "Step 1.3: deploy firestore rules"
    firebase deploy --only firestore

    green_echo "Step 1.4: deploy storage rules"
    firebase deploy --only storage

    green_echo "Step 1.5: deploy firebase functions without confirmation for delete"
    firebase deploy -f --only functions
}

# Define function to install on emulator and run
deploy_frontend () {
    green_echo "Step 2: Deploying to emulator"
    cd $CURRENT_PATH/frontend/Gossip

    green_echo "Step 2.0: Give gradlew executable permissions"
    chmod +x ./gradlew

    green_echo "Step 2.1: Build and install to running emulator"
    ./gradlew installDebug

    green_echo "Step 2.2: Run on emulator using adb"
    adb shell am start -n "org.aalto.mcc.gossip/org.aalto.mcc.gossip.MainActivity" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER

    green_echo "Step 2.3: Start logging and print warnings from logcat"
    adb logcat *:W
}

print_help() {
    echo "Usage:"
    echo -e "\033[00;32m./deploy -h\033[0m: Shows available commands."
    echo -e "\033[00;32m./deploy --only [backend|frontend]\033[0m: Deploys only backend or frontend."
}

green_echo "Group 3 deploy script."

if [[ $# -eq 0 ]]
    then
        deploy_backend
        deploy_frontend
elif [[ $# -eq 1 ]]
    then
        if [[ $1 = "-h" ]]
            then
                print_help
                exit 0
        elif [[ $1 = "--only" ]]
            then
                echo -e "\033[00;31mMissing parameter.\033[0m Usage: ./deploy --only [backend|frontend]"
                exit 1
        else
            echo -e "\033[00;31mInvalid parameter.\033[0m"
            print_help
            exit 1
        fi
elif [[ $# -eq 2 ]]
    then
        if [[ $1 = "--only" && $2 = "frontend" ]]
            then
                deploy_frontend
        elif [[ $1 = "--only" && $2 = "backend" ]]
            then
                deploy_backend
        else
            echo -e "\033[00;31mInvalid parameter.\033[0m Usage: ./deploy --only [backend|frontend]"
            exit 1
        fi
else
    echo -e "\033[00;31mInvalid parameters.\033[0m"
    print_help
    exit 1
fi